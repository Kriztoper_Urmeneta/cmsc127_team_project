package cmsc127.project.upseis.frame;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ColorModel;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cmsc127.project.upseis.controller.InventoryController;
import cmsc127.project.upseis.panels.ManageItemsPanel;
import cmsc127.project.upseis.panels.BorrowersFormPanel;
import cmsc127.project.upseis.panels.BorrowersHistoryPanel;
import cmsc127.project.upseis.panels.BorrowersListPanel;
import cmsc127.project.upseis.panels.ChangeAdminPanel;
import cmsc127.project.upseis.panels.DialogPanel;
import cmsc127.project.upseis.panels.ItemsHistoryPanel;
import cmsc127.project.upseis.panels.LoginPanel;
import cmsc127.project.upseis.panels.ViewItemsFrame;

import javax.swing.JSplitPane;

public class InventoryView extends JFrame {

	private boolean itemsFrameOpen;
	
	private String currentStudentNum;
	private String operationName="";
	
	private JButton borrowersListButton;
	private JButton manageItemsButton;
	private JButton viewItemsButton;
	private JButton previousButton; //keeps track of the selected button for changing the color
	private JButton logoutButton;
	
	private DialogPanel dialogPanel;
	
	private LoginPanel loginPanel;
	private ChangeAdminPanel changeAdminPanel;
	private JSplitPane splitPane;
	private BorrowersFormPanel borrowersFormPanel;
	private BorrowersListPanel borrowersListPanel;
	private ManageItemsPanel manageItemsPanel;
	private ItemsHistoryPanel itemsHistoryPanel;
	private BorrowersHistoryPanel borrowersHistoryPanel;
	
	private JLabel logoLabel;
	
	private JPanel buttonsPanel;
	private JPanel cardsPanel;
	private JPanel blankPanel;
	
	private ViewItemsFrame viewItemsFrame;
	
	private CardLayout cards;
	
	private InventoryController controller;
	
	public InventoryView() {
		initFrame();
		
		initController();
		initViewItemsFrame();
		initLoginPanel();
		initButtonsPanel();
		initCardsPanel();
		initSplitPane();
		initButtonListeners();
	}
	
	public void initFrame() { 
		setTitle("UPVTC Sports Equipment Inventory System (UP SEIS)");
		setResizable(false);
		setSize(980, 580);
		setLocationRelativeTo(null);
		//setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	controller.closeDBConnection();
		    	System.exit(0);
		    }
		});
	}
	
	public void initController() {
		controller = new InventoryController();
	}
	
	public void initViewItemsFrame() {
		viewItemsFrame = new ViewItemsFrame();
		itemsFrameOpen = false;
	}
	
	public void initLoginPanel() {
		dialogPanel = new DialogPanel();
		loginPanel = new LoginPanel();
		changeAdminPanel = new ChangeAdminPanel();
		getContentPane().add(loginPanel);
	}
	
	public void initButtonsPanel() {
		Image img;
		
		// init buttons panel
		buttonsPanel = new JPanel(null);
		buttonsPanel.setBackground(new Color(75, 75, 75));
		buttonsPanel.setSize(200, 580);
		
		// init components
		
		manageItemsButton = new JButton();
		manageItemsButton.setBounds(4, 10, 192, 40);

		try {
			img = ImageIO.read(getClass().getResource("/manageitems.png"));
			manageItemsButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/manageitemsclicked.png"));
			manageItemsButton.setSelectedIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		viewItemsButton = new JButton();
		viewItemsButton.setBounds(4, 50, 192, 40);
		try {
			img = ImageIO.read(getClass().getResource("/view.png"));
			viewItemsButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/viewclicked.png"));
			viewItemsButton.setSelectedIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		borrowersListButton = new JButton();
		borrowersListButton.setBounds(4, 120, 192, 40);
		try {
			img = ImageIO.read(getClass().getResource("/borrowerslist.png"));
			borrowersListButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/borrowersclicked.png"));
			borrowersListButton.setSelectedIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		logoutButton = new JButton();
		logoutButton.setBounds(4, 500, 192, 40);
		try {
			img = ImageIO.read(getClass().getResource("/logout.png"));
			logoutButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/logoutclicked.png"));
			logoutButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//init previousButton
		previousButton = manageItemsButton;
		
		buttonsPanel.add(borrowersListButton);
		buttonsPanel.add(manageItemsButton);
		buttonsPanel.add(viewItemsButton);
		buttonsPanel.add(logoutButton);
		
		JLabel lblNote = new JLabel("<html>&nbsp;&nbsp;NOTE:"
				+ "<br />&emsp;&emsp;Please make sure"
				+ "<br />&emsp;&emsp;that you are always"
				+ "<br />&emsp;&emsp;connected to the"
				+ "<br />&emsp;&emsp;internet to run"
				+ "<br />&emsp;&emsp;this application.</html>");
		lblNote.setOpaque(true);
		lblNote.setBackground(new Color(174, 232, 237));
		lblNote.setBounds(4, 190, 192, 120);
		buttonsPanel.add(lblNote);
	}
	
	public void initCardsPanel() {
		Image img;
		// init cards panel
		cardsPanel = new JPanel();
		cards = new CardLayout();
		cardsPanel.setLayout(cards);
		
		// init components
		blankPanel = new JPanel();
		blankPanel.setLayout(null);
		
		logoLabel = new JLabel();
		logoLabel.setBounds(0, 0, 772, 549);
		try {
			img = ImageIO.read(getClass().getResource("/welcomepanelcenter.png"));
			logoLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		blankPanel.add(logoLabel);
		
		borrowersFormPanel = new BorrowersFormPanel();
		borrowersListPanel = new BorrowersListPanel();
		manageItemsPanel = new ManageItemsPanel();
		itemsHistoryPanel = new ItemsHistoryPanel();
		borrowersHistoryPanel = new BorrowersHistoryPanel();
		
		cardsPanel.add(blankPanel, "blankPanel");
		cardsPanel.add(borrowersFormPanel, "borrowersForm");
		cardsPanel.add(borrowersListPanel, "borrowersList");
		cardsPanel.add(manageItemsPanel, "borrowItems");
		cardsPanel.add(itemsHistoryPanel, "itemsHistory");
		cardsPanel.add(borrowersHistoryPanel, "borrowersHistory");
		cards.show(cardsPanel, "blankPanel");
	}
	
	public void initSplitPane() {
		buttonsPanel.setMinimumSize(new Dimension(200, 580));
		
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, buttonsPanel, cardsPanel);
		splitPane.setDividerSize(0);
	}
	
	public InventoryView getInventoryView() {
		return this;
	}
	
	public void initButtonListeners() {
		changeAdminPanel.getCreateAccButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> adminNumber = (controller.getAdministratorsEmpNum());
				String inputFirst =(changeAdminPanel.getEmpFirstNameField().getText());
				String inputLast = (changeAdminPanel.getEmpLastNameField().getText());
				String inputEmpNum = (changeAdminPanel.getEmpNumField().getText());
				String inputPassword = new String(changeAdminPanel.getEmpPassField().getPassword());
				String inputrePass = new String(changeAdminPanel.getRetypePassField().getPassword());
							
				if(changeAdminPanel.isComplete(inputFirst, inputLast, inputEmpNum, inputPassword, inputrePass)) {
					if (!changeAdminPanel.checkExistingAdmin(adminNumber, inputEmpNum)) {
						dialogPanel.showDialog(getInventoryView(), "Error Message", 
								"<html>Employee number already exists.<br>Try changing it.</html>");
					} else if(changeAdminPanel.nameIsOk(inputFirst) 
							&& changeAdminPanel.nameIsOk(inputLast) 
							&& changeAdminPanel.checkEmpNum(inputEmpNum) 
							&& changeAdminPanel.checkEqual(inputPassword, inputrePass)
							&& changeAdminPanel.checkExistingAdmin(adminNumber, inputEmpNum)) {
						
						controller.setCurrentAdminToInactive();
						controller.insertAdministrator(changeAdminPanel.getAdministratorInfo());
					
						changeAdminPanel.setVisible(false);
						getContentPane().remove(changeAdminPanel);
						loginPanel.clearComponents();
						loginPanel.setVisible(true);
						getContentPane().add(loginPanel);
						
						dialogPanel.showDialog(getInventoryView(), "Information Message", 
								"New admin account created.");
					} else {
						dialogPanel.showDialog(getInventoryView(), "Error Message", 
								"Oops! Wrong input.");
					}
				} else {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"Incomplete input!");
				}
			}
		});
		
		changeAdminPanel.getCancelButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				changeAdminPanel.clearComponents();
				changeAdminPanel.setVisible(false);
				getContentPane().remove(changeAdminPanel);
				loginPanel.clearComponents();
				loginPanel.setVisible(true);
				getContentPane().add(loginPanel);
			}
		});
		
		logoutButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				splitPane.setVisible(false);
				getContentPane().remove(splitPane);
				loginPanel.clearComponents();
				loginPanel.setVisible(true);
				getContentPane().add(loginPanel);
			}
		});
		
		loginPanel.getChangeAdminButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String adminNumber = (controller.getCurrentAdminNumber().get(0));
				String adminPassword = (controller.getCurrentAdminPassword().get(0));
				String inputNumber = loginPanel.getEmpNumberField().getText();
				String inputPassword = new String(loginPanel.getEmpPasswordField().getPassword());
				
				if ((adminNumber.equals(inputNumber))
						&& (adminPassword.equals(inputPassword))) {
					loginPanel.clearComponents();
					loginPanel.setVisible(false);
					getContentPane().remove(loginPanel);
					changeAdminPanel.clearComponents();
					changeAdminPanel.setVisible(true);
					getContentPane().add(changeAdminPanel);
				} else if ((!adminNumber.equals(inputNumber)
						|| (!adminPassword.equals(inputPassword)))) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"Oops! Wrong admin information.");
				}
			}
		});
		
		loginPanel.getLoginButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String adminNumber = (controller.getCurrentAdminNumber().get(0));
				String adminPassword = (controller.getCurrentAdminPassword().get(0));
				String inputNumber = loginPanel.getEmpNumberField().getText();
				String inputPassword = new String(loginPanel.getEmpPasswordField().getPassword());
				
				if ((adminNumber.equals(inputNumber))
						&& (adminPassword.equals(inputPassword))) {
					loginPanel.clearComponents();
					loginPanel.setVisible(false);
					getContentPane().remove(loginPanel);
					splitPane.setVisible(true);
					getContentPane().add(splitPane);
				} else if ((!adminNumber.equals(inputNumber)
						|| (!adminPassword.equals(inputPassword)))) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
						"Oops! Wrong admin information.");
				}
			}
		});
		
		borrowersListButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				previousButton.setSelected(false);
				borrowersListButton.setSelected(true);
				
				borrowersListPanel.clearTextFields();
				String[] columnNames = {"firstName","lastName","studentNum","contactNum","teamName"};
				borrowersListPanel.setItemsTable(controller.selectBorrowersList(), columnNames);
				cards.show(cardsPanel, "borrowersList");
				
				previousButton = borrowersListButton;
			}
		});		

		borrowersListPanel.getAddBorrowerButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				borrowersFormPanel.initComponents();
				cards.show(cardsPanel, "borrowersForm");

				borrowersFormPanel.getCancelButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String[] columnNames = {"firstName","lastName","studentNum","contactNum","teamName"};
						borrowersListPanel.setItemsTable(controller.selectBorrowersList(), columnNames);
						cards.show(cardsPanel, "borrowersList");
					}
				});
				
				borrowersFormPanel.getBorrowButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						ArrayList<String> studentNumbers = (controller.studentNumbers());
						String inputFirst = borrowersFormPanel.getFirstNameField().getText();
						String inputLast = borrowersFormPanel.getLastNameField().getText();
						String inputStudNum = borrowersFormPanel.getStudentNumField().getText();
						String inputContNum = borrowersFormPanel.getContactNumField().getText();
						String inputTeamName = borrowersFormPanel.getTeamNameField().getText();
						
						if(borrowersFormPanel.isComplete(inputFirst, inputLast, inputStudNum, inputContNum, inputTeamName)) {
							if( borrowersFormPanel.checkExistingStudNum(studentNumbers, inputStudNum)) {
								if(borrowersFormPanel.nameIsOk(inputFirst) 
										&& borrowersFormPanel.nameIsOk(inputLast) 
										&& borrowersFormPanel.checkStudNum(inputStudNum) 
										&& borrowersFormPanel.checkContNum(inputContNum)) {
								
									dialogPanel.showDialog(getInventoryView(), "Information Message", 
											"New borrower account created.");
									controller.createBorrower(borrowersFormPanel.getBorrowerInfo());
									borrowersFormPanel.clearComponents();
									previousButton.setSelected(false);
									borrowersListButton.setSelected(true);
									
									String[] columnNames = {"firstName","lastName","studentNum","contactNum","teamName"};
									borrowersListPanel.setItemsTable(controller.selectBorrowersList(), columnNames);
									cards.show(cardsPanel, "borrowersList");
									
									previousButton = borrowersListButton;
								} else {
									dialogPanel.showDialog(getInventoryView(), "Error Message", 
											"Oops! Invalid Input!");
								}
							} else {
								dialogPanel.showDialog(getInventoryView(), "Error Message", 
										"Borrower already exist!");
							}
						}  else {
							dialogPanel.showDialog(getInventoryView(), "Error Message", 
									"Oops! You lack some inputs.");
						}
					}
				});
			}
		});
		
		borrowersListPanel.getDeleteBorrowersButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.deleteBorrowers(borrowersListPanel.deleteBorrowers());
				if (borrowersListPanel.getNoCellEditsCount() == 5) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>You must select row(s)<br>to delete.</html>");	
					borrowersListPanel.setNoCellEditsCount(0);
				} else {
					borrowersListPanel.clearTextFields();
				}
			}
		});
		
		borrowersListPanel.getEditBorrowerButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> studentNumbers = (controller.studentNumbers());
				if (!borrowersListPanel.checkStudentNum(studentNumbers, 
						borrowersListPanel.getStudentNumField().getText())) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>Student number already exists.<br>"
							+ "Try using other student numbers.</html>");
				} else {
					
					if (borrowersListPanel.isempty() 
							&& borrowersListPanel.getNoCellEditsCount() == 5) {
						dialogPanel.showDialog(getInventoryView(), "Error Message", 
								"<html>Please enter your inputs<br>on the fields.</html>");	
					}
					
					ArrayList<String> borrower = borrowersListPanel.editBorrower();

					if (borrower != null) {
						if (borrower.isEmpty()) {
							dialogPanel.showDialog(getInventoryView(), "Error Message", 
									"<html>Please select row to edit<br>"
									+ "then enter inputs to the fields.</html>");
						} else {
							if(!borrowersListPanel.nameIsOk(borrower.get(0))) {
								dialogPanel.showDialog(getInventoryView(), "Error Message", 
										"Invalid First Name!");
							} else if(!borrowersListPanel.nameIsOk(borrower.get(1))) {
								dialogPanel.showDialog(getInventoryView(), "Error Message", 
										"Invalid Last Name!");
							} else if(!borrowersListPanel.checkStudNum(borrower.get(2))) {
								dialogPanel.showDialog(getInventoryView(), "Error Message", 
										"Invalid Student Number!");
							} else if(!borrowersListPanel.checkContNum(borrower.get(3))) {
								dialogPanel.showDialog(getInventoryView(), "Error Message", 
										"Invalid Contact Number!");
							} else {
								controller.editBorrower(borrower);
								borrowersListPanel.insertToTable(borrower);
							}
						}
					}
				}
			}
		});
		
		borrowersListPanel.getBorrowersHistoryButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				borrowersHistoryPanel.setItemsTable();
				cards.show(cardsPanel, "borrowersHistory");
				
				borrowersHistoryPanel.getSearchButton().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						String searchStudNum = borrowersHistoryPanel.getSearchField().getText();
						borrowersHistoryPanel.setItemsTable(controller.selectBorrowersHistory(searchStudNum));
					}
				});
				
				borrowersHistoryPanel.getBackButton().addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						String[] columnNames = {"firstName","lastName","studentNum","contactNum","teamName"};
						borrowersListPanel.setItemsTable(controller.selectBorrowersList(), columnNames);
						cards.show(cardsPanel, "borrowersList");
					}
				});
			}
		});
		
		manageItemsPanel.getBorrowItemsButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentStudentNum.isEmpty()) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>Current borrower<br>not yet selected!</html>");
				}
			}
		});
		
		manageItemsPanel.getReturnItemsButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (manageItemsPanel.hasItemsToReturnSelected()) {//operationName.equals("return") && !manageItemsPanel.itemsTableIsEmpty()) {
					String returnedItemsID = controller.returnSelectedItems(manageItemsPanel.getSelectedItemsToReturn());
					dialogPanel.showDialog(getInventoryView(), "Information Message", 
							"Return success!");
					manageItemsPanel.setBorrowedItemsTable(controller.getBorrowedItems(currentStudentNum));
					controller.setDateReturned(returnedItemsID);
					manageItemsPanel.clearItemsTable();
					operationName = "";
				} else {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>No items borrowed are selected!</html>");
				}
			}
		});
		
		manageItemsPanel.getReserveItemsButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentStudentNum.isEmpty()) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>Current borrower<br>not yet selected!</html>");
				}
			}
		});

		manageItemsPanel.getSearchButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (manageItemsPanel.hasItemsSelected() && currentStudentNum.isEmpty()) {
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>Current borrower<br>not yet selected!</html>");
				}
			}
		});
		
		manageItemsButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				previousButton.setSelected(false);
				manageItemsButton.setSelected(true);
				
				manageItemsPanel.clearComponents();
				currentStudentNum = "";
				cards.show(cardsPanel, "borrowItems");
				
				manageItemsPanel.getSearchBorrowersButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						currentStudentNum = manageItemsPanel.getSearchBorrowersField().getText();
						//manageItemsPanel.getSearchBorrowersField().setText("");
						if (!currentStudentNum.isEmpty()) {
							if(!controller.getBorrower(currentStudentNum).isEmpty()) {
								manageItemsPanel.setBorrowersTable(controller.getBorrower(currentStudentNum));
								manageItemsPanel.setBorrowedItemsTable(controller.getBorrowedItems(currentStudentNum));
							
								manageItemsPanel.getSearchButton().addActionListener(new ActionListener() {
										
									@Override
									public void actionPerformed(ActionEvent e) {
										if (manageItemsPanel.getSelectedRadio().equals("borrow")
												&& !currentStudentNum.equals("")) {
											String itemName = manageItemsPanel.getSearchItemsField().getText();
											manageItemsPanel.setItemsTable(controller.getAvailableOrReservedItems(itemName, currentStudentNum));
											operationName = "borrow";
										} else if (manageItemsPanel.getSelectedRadio().equals("reserve")
												&& !currentStudentNum.equals("")) {
											String itemName = manageItemsPanel.getSearchItemsField().getText();
											manageItemsPanel.setItemsTable(controller.getAvailableItems(itemName));
											operationName = "reserve";
										}
										
										manageItemsPanel.getBorrowItemsButton().addActionListener(new ActionListener() {
											
											@Override
											public void actionPerformed(ActionEvent e) {
												if (operationName.equals("borrow") && !manageItemsPanel.itemsTableIsEmpty()) {
													controller.borrowSelectedItems(manageItemsPanel.getSelectedItems(), currentStudentNum);
													dialogPanel.showDialog(getInventoryView(), "Information Message", 
															"Borrow success!");
													controller.insertToItemsHistory(manageItemsPanel.getSelectedItems(), currentStudentNum);
													manageItemsPanel.setBorrowedItemsTable(controller.getBorrowedItems(currentStudentNum));
													manageItemsPanel.clearItemsTable();
													operationName = "";
												} else if (!operationName.equals("borrow") && !manageItemsPanel.itemsTableIsEmpty()) {
													dialogPanel.showDialog(getInventoryView(), "Error Message", 
															"<html>Oops! You maybe mean to<br>reserve items.</html>");
												}
											}
										});
										
										manageItemsPanel.getReserveItemsButton().addActionListener(new ActionListener() {
											
											@Override
											public void actionPerformed(ActionEvent e) {
												if (operationName.equals("reserve") && !manageItemsPanel.itemsTableIsEmpty()) {
													controller.reserveSelectedItems(manageItemsPanel.getSelectedItems(), currentStudentNum);
													dialogPanel.showDialog(getInventoryView(), "Information Message", 
															"Reserve Success!");
													manageItemsPanel.clearItemsTable();
													operationName = "";
												} else if (!operationName.equals("reserve") && !manageItemsPanel.itemsTableIsEmpty()) {
													dialogPanel.showDialog(getInventoryView(), "Error Message", 
															"<html>Oops! You maybe mean to<br>borrow items.</html>");
												}
											}
										});
									}
								});
							} else {
								dialogPanel.showDialog(getInventoryView(), "Error Message", 
										"<html>Wrong input or<br>Account does not exist!</html>");
								manageItemsPanel.clearTables();
								currentStudentNum = "";
							}
						} else {
							dialogPanel.showDialog(getInventoryView(), "Error Message", 
									"<html>Wrong input or<br>Account does not exist!</html>");
							manageItemsPanel.clearTables();
							currentStudentNum = "";
						}
					}
				});
				
				previousButton = manageItemsButton;
			}
		});
		
		viewItemsButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				previousButton.setSelected(false);
				viewItemsButton.setSelected(true);
				
				if (!itemsFrameOpen) {
					initViewItemsFrame();
					initButtonListeners();
					String[] columnNames = {"Item Number","Item Name","State"};
					viewItemsFrame.setItemsTable(controller.viewItems(), columnNames);
					viewItemsFrame.setVisible(true);
					itemsFrameOpen = true;
				} else {
					viewItemsFrame.toFront();
				}
				
				viewItemsFrame.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				    	viewItemsFrame.dispose();
				    	itemsFrameOpen = false;
				    }
				});
				
				previousButton = viewItemsButton;
			}
		});
		
		viewItemsFrame.getEditButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> value = viewItemsFrame.editItem();
				if(viewItemsFrame.isempty())
					dialogPanel.showDialog(getInventoryView(), "Error Message", 
							"<html>Please enter your inputs<br>on the fields.</html>");
				if (value != null) {
					if (value.isEmpty()) {
						dialogPanel.showDialog(getInventoryView(), "Error Message", 
								"<html>Please select row to edit<br>"
								+ "then enter inputs to the fields.</html>");
					} else {
						if(!viewItemsFrame.checkItemNumber(value.get(0))) {
							dialogPanel.showDialog(viewItemsFrame, "Error Message", 
									"Invalid Item Number!");
						} else if(viewItemsFrame.checkItemNum(controller.selectItemNum(), value.get(0)) || value.get(0).equals(value.get(3))) {
							controller.editItem(value);
							viewItemsFrame.insertToTable(value);
						} else {
							dialogPanel.showDialog(viewItemsFrame, "Error Message", 
									"<html>Item Number Already<br>Exist!</html>");
						}
					}
					if (viewItemsFrame.getNoCellEditsCount() == 2) {
						dialogPanel.showDialog(getInventoryView(), "Error Message", 
								"<html>Please enter your inputs<br>on the fields.</html>");	
						viewItemsFrame.setNoCellEditsCount(0);
					}
				} else {
					dialogPanel.showDialog(viewItemsFrame, "Error Message", 
							"Invalid or Incomplete Input!");
				}
			}
		});
		
		viewItemsFrame.getDeleteButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> itemNums = viewItemsFrame.deleteItem();
				if (itemNums != null) {
					controller.deleteItems(itemNums);
				} else {
					dialogPanel.showDialog(viewItemsFrame, "Error Message", 
							"<html>You must select row(s)<br>to delete.</html>");
				}
			}
		});
		
		viewItemsFrame.getNewItemButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				//if(viewItemsFrame.checkItemNumber(itemNumber) && viewItemsFrame.checkItemName(itemName) && !itemNumber.isEmpty() && !itemName.isEmpty())
				String[] itemValues = viewItemsFrame.addItem();
				if (itemValues != null) {
					if(viewItemsFrame.checkItemNumber(itemValues[0])) {
						if(viewItemsFrame.checkItemNum(controller.selectItemNum(), itemValues[0])) {
							controller.addItem(itemValues);
							viewItemsFrame.addToTable(itemValues);
						} else {
							dialogPanel.showDialog(viewItemsFrame, "Error Message", 
									"<html>Item Number Already<br>Exist!</html>");
						}
					} else {
						dialogPanel.showDialog(viewItemsFrame, "Error Message", 
								"Invalid Item Number!");
					}
				} else {
						dialogPanel.showDialog(viewItemsFrame, "Error Message", 
								"Invalid or Incomplete Input!");
				}
			}
		});
		
		viewItemsFrame.getItemsHistoryPanel().getSearchButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String searchItemNum = viewItemsFrame.getItemsHistoryPanel().getSearchField().getText();
				viewItemsFrame.getItemsHistoryPanel().setItemsTable(controller.selectItemsHistory(searchItemNum));
			}
		});
	}
}
