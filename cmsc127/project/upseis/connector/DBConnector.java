package cmsc127.project.upseis.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class DBConnector {
	// JDBC driver name and database URL
		static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
		static final String DB_URL = "jdbc:mysql://52.74.77.8:3306/";//localhost/";
		//sql6.freesqldatabase.com = 52.74.77.8

		//  Database credentials
		static final String USER = "sql6152352";//"cmsc127";
		static final String PASS = "QVlVdNddDL";//"password";
		   
		private Connection conn = null;
		private Statement stmt = null;
		
		public DBConnector() {
			//STEP 2: Register JDBC driver
			try {
				Class.forName("com.mysql.jdbc.Driver");
				
				//STEP 3: Open a connection
				Properties p = new Properties();
				p.put("user",USER);
				p.put("password",PASS);
				p.put("useSSL", "false");
				//conn = DriverManager.getConnection(DB_URL+"sql6152352", p);//"mysql", p);
				
				//STEP 4: Execute a query
				//stmt = conn.createStatement();
				
				//Create database if it does not exist
				//String sql = "CREATE DATABASE IF NOT EXISTS sql6152352"; //up_seis";
				//stmt.executeUpdate(sql);
				
				conn = DriverManager.getConnection(DB_URL+"sql6152352", p);//"up_seis", p);
				stmt = conn.createStatement();
				
				//Create table administrators if it does not exist
				String sql = "CREATE TABLE IF NOT EXISTS administrators ("
					+ "empFirstName varchar(35) NOT NULL, "
					+ "empLastName varchar(35) NOT NULL, "
					+ "empNumber varchar(30) NOT NULL, "
					+ "empPassword varchar(70) NOT NULL, "
					+ "activity enum('active','inactive') NOT NULL DEFAULT 'active', "
					+ "PRIMARY KEY(empNumber));";
				stmt.executeUpdate(sql);
				ResultSet res = stmt.executeQuery("SELECT * FROM administrators");
				if (res.next() == false) {
					sql = "INSERT INTO administrators (empFirstName,empLastName,empNumber,"
						+ "empPassword) VALUES ('','','','')";
					stmt.executeUpdate(sql);
				}
				
				//Create table borrowers if it does not exist
				sql = "CREATE TABLE IF NOT EXISTS borrowers ("
					+ "firstName varchar(30) NOT NULL, "
					+ "lastName varchar(30) NOT NULL, "
					+ "studentNum varchar(9) NOT NULL, "
					+ "contactNum varchar(30) NOT NULL, "
					+ "teamName varchar(30) NOT NULL, "
					+ "PRIMARY KEY(studentNum));";
				stmt.executeUpdate(sql);
				
				//Create table items if it does not exist
				sql = "CREATE TABLE IF NOT EXISTS items ("
					+ "itemNum varchar(30) NOT NULL, "
					+ "itemName varchar(30) NOT NULL, "
					+ "state enum('intact','damaged') NOT NULL, "
					+ "reservationID varchar(9) DEFAULT NULL, "
					+ "borrowerID varchar(9) DEFAULT NULL, "
					+ "PRIMARY KEY(itemNum),"
					+ "FOREIGN KEY(reservationID) REFERENCES borrowers(studentNum),"
					+ "FOREIGN KEY(borrowerID) REFERENCES borrowers(studentNum));";
				stmt.executeUpdate(sql);
				
				//Create table borrowers if it does not exist
				sql = "CREATE TABLE IF NOT EXISTS items_history ("
					+ "id int(11) NOT NULL AUTO_INCREMENT, "
					+ "itemNum varchar(30) NOT NULL, "
					+ "itemName varchar(30) NOT NULL, "
					+ "state enum('intact','damaged') NOT NULL, "
					+ "borrowerName varchar(70) NOT NULL, "
					+ "dateBorrowed timestamp NOT NULL,"
					+ "dateReturned timestamp NULL, "
					+ "borrowerID varchar(11) NOT NULL, "
					+ "PRIMARY KEY(id));";
				stmt.executeUpdate(sql);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public ArrayList<String> executeQuery(String sql, String event) {
			ArrayList<String> list = new ArrayList<String>();
			
			try{
				ResultSet rs = stmt.executeQuery(sql);
				
				//STEP 5: Extract data from result set
				if (event.equals("view items")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						String itemName = rs.getString("itemName");
						String state = rs.getString("state");
						
						//Assign the values to arraylist
						list.add(itemNum+","+itemName+","+state);
					}
				} else if (event.equals("select itemsNum")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						list.add(itemNum);
					}
				} else if (event.equals("select studentNum")) {
					while(rs.next()){
						//Retrieve by column name
						String studNum = rs.getString("studentNum");
						list.add(studNum);
					}
				} else if (event.equals("select items history")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						String itemName = rs.getString("itemName");
						String state = rs.getString("state");
						String borrowerName = rs.getString("borrowerName");
						String dateBorrowed = rs.getString("dateBorrowed");
						String dateReturned = rs.getString("dateReturned");
						
						//Assign the values to arraylist
						list.add(itemNum+","+itemName+","+state+","
							+borrowerName+","+dateBorrowed+","+dateReturned);
					}
				} else if (event.equals("select borrowers history")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						String itemName = rs.getString("itemName");
						String state = rs.getString("state");
						String borrowerName = rs.getString("borrowerName");
						String dateBorrowed = rs.getString("dateBorrowed");
						String dateReturned = rs.getString("dateReturned");
						
						//Assign the values to arraylist
						list.add(borrowerName+","+itemName+","+itemNum+","+state
								+","+dateBorrowed+","+dateReturned);
					}
				} else if (event.equals("select borrowers list")) {
					while(rs.next()){
						//Retrieve by column name
						String firstName = rs.getString("firstName");
						String lastName = rs.getString("lastName");
						String studentNum = rs.getString("studentNum");
						String contactNum = rs.getString("contactNum");
						String teamName = rs.getString("teamName");
						
						//Assign the values to arraylist
						list.add(firstName+","+lastName+","+studentNum+","
							+contactNum+","+teamName);
					}
				} else if (event.equals("select current employee number")) {
					while(rs.next()){
						//Retrieve by column name
						String empNumber = rs.getString("empNumber");
						
						//Assign the values to arraylist
						list.add(empNumber);
					}
				} else if (event.equals("select borrowers number")) {
					while(rs.next()){
						//Retrieve by column name
						String studNum = rs.getString("studentNum");
						
						//Assign the values to arraylist
						list.add(studNum);
					}
				} else if (event.equals("select current employee password")) {
		
					while(rs.next()){
						//Retrieve by column name
						String empPassword = rs.getString("empPassword");
						
						//Assign the values to arraylist
						list.add(empPassword);
					}
				} else if (event.equals("select current employee")) {
					while(rs.next()){
						//Retrieve by column name
						String empFirstName = rs.getString("empFirstName");
						String empLastName = rs.getString("empLastName");
						String empNumber = rs.getString("empNumber");
						String empPassword = rs.getString("empPassword");
						String activity = rs.getString("activity");
						
						//Assign the values to arraylist
						list.add(empPassword);
					}
				} else if(event.equals("select existing employee number")) {
					while(rs.next()){
						//Retrieve by column name
						String empNumber = rs.getString("empNumber");
						
						list.add(empNumber);
					}
				} else if (event.equals("select borrowed items by current borrower")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						String itemName = rs.getString("itemName");
						String state = rs.getString("state");
						
						//Assign the values to arraylist
						list.add(itemNum+","+itemName+","+state);
					}
				} else if (event.equals("select borrowed items by current borrower (all columns)")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						String itemName = rs.getString("itemName");
						String state = rs.getString("state");
						String reservationID = rs.getString("reservationID");
						String borrowerID = rs.getString("borrowerID");
						
						//Assign the values to arraylist
						list.add(itemNum+","+itemName+","+state+","+reservationID+","+borrowerID);
					}
				} else if (event.equals("select available items")) {
					while(rs.next()){
						//Retrieve by column name
						String itemNum = rs.getString("itemNum");
						String itemName = rs.getString("itemName");
						String state = rs.getString("state");
						String reservationID = rs.getString("reservationID");
						String borrowerID = rs.getString("borrowerID");
						
						//Assign the values to arraylist
						list.add(itemNum+","+itemName+","+state+","+reservationID+","+borrowerID);
					}
				}else if (event.equals("select student name")) {
					while(rs.next()){
						//Retrieve by column name
						String studentName = rs.getString("studentName");
						
						//Assign the values to arraylist
						list.add(studentName);
					}
				} else if (event.equals("select borrower")) {
					while(rs.next()){
						//Retrieve by column name
						String firstName = rs.getString("firstName");
						String lastName = rs.getString("lastName");
						String studentNum = rs.getString("studentNum");
						String contactNum = rs.getString("contactNum");
						String teamName = rs.getString("teamName");
						
						//Assign the values to arraylist
						list.add(firstName+","+lastName+","+studentNum+","+contactNum+","+teamName);
					}
				}

				rs.close();
			} catch(SQLException se){
				//Handle errors for JDBC
				se.printStackTrace();
			} catch(Exception e){
				//Handle errors for Class.forName
				e.printStackTrace();
			}
			
			return list;
		}
		
		public void close() {
			//STEP 6: Clean-up environment
			try {
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		public void executeUpdate(String sql){
			try{
				stmt.executeUpdate(sql);
			} catch(SQLException se){
				//Handle errors for JDBC
				se.printStackTrace();
			} catch(Exception e){
				//Handle errors for Class.forName
				e.printStackTrace();
			}
		}
}
