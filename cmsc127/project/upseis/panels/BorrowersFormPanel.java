package cmsc127.project.upseis.panels;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cmsc127.project.upseis.infoclasses.BorrowerInfo;

import java.awt.Color;
import java.util.ArrayList;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;

public class BorrowersFormPanel extends JPanel {

	private JLabel nameLabel;
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JLabel studentNumLabel;
	private JLabel contactNumLabel;
	private JLabel teamNameLabel;
	
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField studentNumField;
	private JTextField contactNumField;
	private JTextField teamNameField;
	
	private JButton createButton;
	private JButton cancelButton;
	
	private Image img;
	
	private BorrowerInfo borrowerInfo;
	
	public BorrowersFormPanel() {
		initPanel();
		borrowerInfo = new BorrowerInfo();

		initComponents();
	}
	
	public void initPanel() {
		setLayout(null);
		setBackground(new Color(236, 233, 197));
	}
	
	public void initComponents() {
		removeAll();
		Font txtFont = new Font("Sans", 0, 17);
		Color textColor = new Color(56, 43, 38);
		
		nameLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/namelabel.png"));
			nameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		nameLabel.setBounds(240, 98, 160, 22);
		
		firstNameLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/firstnamelabel.png"));
			firstNameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		firstNameLabel.setBounds(295, 150, 160, 22);
		
		lastNameLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/lastnamelabel.png"));
			lastNameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		lastNameLabel.setBounds(455, 150, 160, 22);
		
		studentNumLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/studentnumberlabel.png"));
			studentNumLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		studentNumLabel.setBounds(240, 175, 160, 22);
		
		contactNumLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/contactnumberlabel.png"));
			contactNumLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		contactNumLabel.setBounds(240, 232, 160, 22);
		
		teamNameLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/teamnamelabel.png"));
			teamNameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		teamNameLabel.setBounds(240, 289, 220, 22);
		
		firstNameField = new JTextField(30);
		firstNameField.setForeground(textColor);
		firstNameField.setFont(txtFont);
		firstNameField.setBackground(new Color(174, 232, 237));
		firstNameField.setBounds(240, 120, 140, 30);
		
		lastNameField = new JTextField(30);
		lastNameField.setForeground(textColor);
		lastNameField.setFont(txtFont);
		lastNameField.setBackground(new Color(174, 232, 237));
		lastNameField.setBounds(400, 120, 140, 30);
		
		studentNumField = new JTextField(30);
		studentNumField.setForeground(textColor);
		studentNumField.setFont(txtFont);
		studentNumField.setBackground(new Color(174, 232, 237));
		studentNumField.setBounds(240, 197, 300, 30);
		
		contactNumField = new JTextField(30);
		contactNumField.setForeground(textColor);
		contactNumField.setFont(txtFont);
		contactNumField.setBackground(new Color(174, 232, 237));
		contactNumField.setBounds(240, 254, 300, 30);
		
		teamNameField = new JTextField(30);
		teamNameField.setForeground(textColor);
		teamNameField.setFont(txtFont);
		teamNameField.setBackground(new Color(174, 232, 237));
		teamNameField.setBounds(240, 311, 300, 30);
		
		createButton = new JButton();
		try {
			img = ImageIO.read(getClass().getResource("/createborrower.png"));
			createButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/createborrowerclicked.png"));
			createButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		createButton.setBounds(410, 425, 150, 40);
		
		cancelButton = new JButton();
		try {
			img = ImageIO.read(getClass().getResource("/cancel.png"));
			cancelButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/cancelclicked.png"));
			cancelButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		cancelButton.setBounds(220, 425, 150, 40);
		
		add(nameLabel);
		add(studentNumLabel);
		add(contactNumLabel);
		add(firstNameLabel);
		add(lastNameLabel);
		add(teamNameLabel);
		add(cancelButton);
		add(createButton);
		
		add(studentNumField);
		add(contactNumField);
		add(firstNameField);
		add(lastNameField);
		add(teamNameField);
	}
	
	public void clearComponents() {
		studentNumField.setText("");
		contactNumField.setText("");
		firstNameField.setText("");
		lastNameField.setText("");
		teamNameField.setText("");
	}
	
	public JTextField getStudentNumField() {
		return studentNumField;
	}
	
	public JTextField getContactNumField() {
		return contactNumField;
	}
	
	public JTextField getFirstNameField() {
		return firstNameField;
	}
	
	public JTextField getLastNameField() {
		return lastNameField;
	}
	
	public JTextField getTeamNameField() {
		return teamNameField;
	}
	
	public JButton getBorrowButton() {
		return createButton;
	}

	public BorrowerInfo getBorrowerInfo() {
		borrowerInfo.setFirstName(getFirstNameField().getText());
		borrowerInfo.setLastName(getLastNameField().getText());
		borrowerInfo.setStudentNum(getStudentNumField().getText());
		borrowerInfo.setContactNum(getContactNumField().getText());
		borrowerInfo.setTeamName(getTeamNameField().getText());
		return borrowerInfo;
	}
	
	public boolean checkExistingStudNum(ArrayList<String> studentNumbers, String studentNumber) {
		for(int i=0; i<studentNumbers.size(); i++) {
			if(studentNumbers.get(i).equals(studentNumber)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isComplete(String firstName, String lastName, String studNum, String contNum, String teamName) {
		if(firstName.equals("") || lastName.equals("") || studNum.equals("") || contNum.equals("") || teamName.equals("")) {
			return false;
		} else 
			return true;
	}
	
	public boolean nameIsOk(String name) {
		boolean allow = true;
		if((name.charAt(0)<='Z'&&name.charAt(0)>='A')) {
			for(int i=1; i<name.length(); i++) {
				if(name.charAt(i)>='a'&&name.charAt(i)<='z'){
				} else {
					allow=false;
					break;
				}
			}
			if(allow)
				return true;
		}
		return false;
	}
	
	public boolean checkContNum(String contactNum) {
		boolean pwede = true;
		if(contactNum.length()==11 && contactNum.charAt(0)=='0' && contactNum.charAt(1)=='9') {
			for(int i=0; i<contactNum.length(); i++) {
				if(contactNum.charAt(i) >= '0' && contactNum.charAt(i) <= '9') {
				} else {
					pwede = false;
				}
			}
			if(pwede) 
				return true;
		}
		return false;
	}
	public boolean checkStudNum(String string) {
		boolean pwede = true;
		if(string.length()==9) {
			for(int i=0; i<string.length(); i++) {
				if(string.charAt(i) >= '0' && string.charAt(i) <= '9') {
				} else {
					pwede = false;
				}
			}
			if(pwede) 
				return true;
		}
		return false;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(JButton cancelButton) {
		this.cancelButton = cancelButton;
	}
}
