package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import cmsc127.project.upseis.infoclasses.ItemInfo;

public class ReserveItemsPanel extends JPanel {

	private final String[] itemsColumnNames = {"Item Number","Item Name","State"};
	private final String[] borrowersColumnNames = {"First Name","Last Name","Student Number",
			"Contact Number", "Team Name"};
	private final String[] borrowedItemsColumnNames = {"Item Number","Item Name","State"};
	
	private JLabel searchItemsLabel;
	private JTextField searchItemsField;
	private JButton searchItemsButton;
	private JTable itemsTable;
	private JButton reserveItemsButton;
	
	private JLabel searchBorrowersLabel;
	private JTextField searchBorrowersField;
	private JButton searchBorrowersButton;
	private JTable borrowersTable;

	private JLabel borrowedItemsLabel;
	private JTable borrowedItemsTable;

	private DefaultTableModel itemsModel;
	private DefaultTableModel bModel;
	private DefaultTableModel biModel;
	private JScrollPane itemsTablePane;
	private JScrollPane borrowersTablePane;
	private JScrollPane borrowedItemsTablePane;

	public ReserveItemsPanel() {
		initPanel();
		
		initComponents();
		addComponents();
	}
	
	public void initPanel() {
		//setLayout(new FlowLayout(FlowLayout.CENTER, 500, 5));
		setBackground(new Color(236, 233, 197));
	}
	
	public void initComponents() {
		searchItemsLabel = new JLabel("Enter item name to search for items");
		setSearchItemsField(new JTextField(30));
		setSearchItemsButton(new JButton("Search"));
		itemsModel = new DefaultTableModel(itemsColumnNames, 0);
		itemsTable = new JTable(itemsModel);
		itemsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		itemsTablePane = new JScrollPane(itemsTable);
		itemsTablePane.setSize(300, 30);
		itemsTable.setPreferredScrollableViewportSize(new Dimension(440, 100));
		setReserveItemsButton(new JButton("Reserve"));
		searchBorrowersLabel = new JLabel("Enter student number to search for borrowers");
		
		setSearchBorrowersField(new JTextField(30));
		
		bModel = new DefaultTableModel(borrowersColumnNames, 0);
		borrowersTable = new JTable(bModel);
		borrowersTablePane = new JScrollPane(borrowersTable);
		borrowersTablePane.setSize(300, 30);
		borrowersTable.setPreferredScrollableViewportSize(new Dimension(440, 100));
		setSearchBorrowersButton(new JButton("Search"));
		borrowedItemsLabel = new JLabel("Borrowed items by current borrower");
		biModel = new DefaultTableModel(borrowedItemsColumnNames, 0);
		borrowedItemsTable = new JTable(biModel);
		borrowedItemsTablePane = new JScrollPane(borrowedItemsTable);
		borrowedItemsTablePane.setSize(300, 30);
		borrowedItemsTable.setPreferredScrollableViewportSize(new Dimension(440, 100));
	}
	
	public void addComponents() {
		removeAll();
		setLayout(new FlowLayout());
		
		setAlignmentX(500);
		setAlignmentY(500);
		//setLayout(null);
		
		add(searchItemsLabel);
		add(searchItemsField);
		add(searchItemsButton);
		add(itemsTablePane);
		add(reserveItemsButton);
		add(searchBorrowersLabel);
		add(searchBorrowersField);
		add(borrowersTablePane);
		add(searchBorrowersButton);
		add(borrowedItemsLabel);
		add(borrowedItemsTablePane);
	}
	
	public void setBorrowersTable(ArrayList<String> borrowerInfo) {
		String[] borrowerInfoArray = borrowerInfo.get(0).split(",");
		bModel.addRow(borrowerInfoArray);
	}
	
	public void setItemsTable(ArrayList<String> itemsInfo) {
		for (String x: itemsInfo) {
			itemsModel.addRow(x.split(","));	
		}
	}
	
	public void setBorrowedItemsTable(ArrayList<String> itemsInfo) {
		for (String x: itemsInfo) {
			biModel.addRow(x.split(","));
		}
	}

	public ArrayList<ItemInfo> getSelectedItems() {
		ArrayList<ItemInfo> itemInfos = new ArrayList<ItemInfo>();
		
		int[] rows = itemsTable.getSelectedRows();

		for (int i = 0; i < rows.length; i++) {
			ItemInfo itemInfo = new ItemInfo();
			itemInfo.setItemNum((String) itemsTable.getValueAt(rows[i], 0));
			itemInfo.setItemName((String) itemsTable.getValueAt(rows[i], 1));
			itemInfo.setState((String) itemsTable.getValueAt(rows[i], 2));
			itemInfos.add(itemInfo);
		}
		
		return itemInfos;
	}
	
	public JTextField getSearchItemsField() {
		return searchItemsField;
	}

	public void setSearchItemsField(JTextField searchItemsField) {
		this.searchItemsField = searchItemsField;
	}

	public JButton getSearchItemsButton() {
		return searchItemsButton;
	}

	public void setSearchItemsButton(JButton searchItemsButton) {
		this.searchItemsButton = searchItemsButton;
	}

	public JButton getReserveItemsButton() {
		return reserveItemsButton;
	}

	public void setReserveItemsButton(JButton reserveItemsButton) {
		this.reserveItemsButton = reserveItemsButton;
	}

	public JTextField getSearchBorrowersField() {
		return searchBorrowersField;
	}

	public void setSearchBorrowersField(JTextField searchBorrowersField) {
		this.searchBorrowersField = searchBorrowersField;
	}

	public JButton getSearchBorrowersButton() {
		return searchBorrowersButton;
	}

	public void setSearchBorrowersButton(JButton searchBorrowersButton) {
		this.searchBorrowersButton = searchBorrowersButton;
	}
}
