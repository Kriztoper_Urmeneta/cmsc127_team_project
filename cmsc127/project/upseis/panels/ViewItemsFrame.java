package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

public class ViewItemsFrame extends JFrame {

	private int noCellEditsCount;
	
	private JPanel leftPanel;
	private ItemsHistoryPanel itemsHistoryPanel;
	
	private JButton editButton;
	private JButton deleteButton;
	private JButton newButton;
	
	private JTable itemsTable;
	
	private Object[][] tableData;
	private String[] columnNames;
	
	private JLabel itemNumLabel;
	private JLabel itemNameLabel;
	private JLabel itemStateLabel;
	
	private JTextField itemNumField;
	private JTextField itemNameField;
	private JComboBox<String> itemStateCombo;
	
	public ViewItemsFrame() {
		initFrame();
		
		initComponents();
		setNoCellEditsCount(0);
	}
	
	public boolean checkItemName(String name) {
		boolean allow = true;
		for(int i=0; i<name.length(); i++) {
			if((name.charAt(i)>='a'&&name.charAt(i)<='z')||(name.charAt(i)>='A'&&name.charAt(i)<='Z')){
			} else {
				allow=false;
				break;
			}
		}
		if(allow)
			return true;
		return false;
	}
	
	public boolean checkItemNumber(String itemNumber) {
		boolean pwede = true;
		for(int i=0; i<itemNumber.length(); i++) {
			if(itemNumber.charAt(i) >= '0' && itemNumber.charAt(i) <= '9') {
			} else {
				pwede = false;
			}
		}
		if(pwede) 
			return true;
		return false;
	}
	
	public boolean checkState(String state) {
		if(state.equals("intact")||state.equals("damaged")) {
			return true;
		}
		return false;
	}
	
	public void initFrame() {
		setTitle("Items Information");
		//setResizable(false);
		setMaximumSize(new Dimension(995, 615));
		setSize(995, 615);
		getContentPane().setBackground(new Color(236, 233, 197));
		setLayout(null);
		setLocationRelativeTo(null);
	}
	
	public void initComponents() {		
		Image img;
		
		leftPanel = new JPanel(null);
		leftPanel.setBackground(new Color(236, 233, 197));
		leftPanel.setBounds(0, 0, 430, 580);
		add(leftPanel);
		
		itemsHistoryPanel = new ItemsHistoryPanel();
		add(itemsHistoryPanel);
		
		editButton = new JButton();
		try {
			img = ImageIO.read(getClass().getResource("/edit.png"));
			editButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/editclicked.png"));
			editButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		editButton.setBounds(10, 530, 130, 40);
		
		deleteButton = new JButton();
		try {
			img = ImageIO.read(getClass().getResource("/delete.png"));
			deleteButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/deleteclicked.png"));
			deleteButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		deleteButton.setBounds(150, 530, 130, 40);
		
		newButton = new JButton();
		try {
			img = ImageIO.read(getClass().getResource("/newitem.png"));
			newButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/newitemclicked.png"));
			newButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		newButton.setBounds(290, 530, 130, 40);
		
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		itemNumLabel = new JLabel("Item Num: ");
		itemNumLabel.setBounds(10, 478, 100, 22);
		
		itemNumField = new JTextField(50);
		itemNumField.setBackground(new Color(174, 232, 237));
		itemNumField.setBounds(10, 500, 130, 25);
		
		itemNameLabel = new JLabel("Item Name: ");
		itemNameLabel.setBounds(150, 478, 100, 22);
		
		itemNameField = new JTextField(50);
		itemNameField.setBackground(new Color(174, 232, 237));
		itemNameField.setBounds(150, 500, 140, 25);
		
		itemStateLabel = new JLabel("Item State: ");
		itemStateLabel.setBounds(300, 478, 100, 22);
		
		String[] state = {"intact","damaged"};
		itemStateCombo = new JComboBox<String>(state);
		itemStateCombo.setBackground(new Color(174, 232, 237));
		itemStateCombo.setBounds(300, 500, 120, 25);
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		leftPanel.add(itemNumLabel);
		leftPanel.add(itemNumField);
		leftPanel.add(itemNameLabel);
		leftPanel.add(itemNameField);
		leftPanel.add(itemStateLabel);
		leftPanel.add(itemStateCombo);
		leftPanel.add(editButton);
		leftPanel.add(deleteButton);
		leftPanel.add(newButton);
	}
	
	public void setItemsTable(ArrayList<String> itemInfos, String[] columnNames) {
		tableData = initData(itemInfos);
		this.columnNames = columnNames;
		itemsTable = new JTable();
		itemsTable.clearSelection();
		itemsTable.setBackground(new Color(174, 232, 237));
		itemsTable.setModel(new MyTableModel(tableData, columnNames));
		itemsTable.setPreferredScrollableViewportSize(new Dimension(660, 520));
        itemsTable.setFillsViewportHeight(true);

        ListSelectionModel listSelectionModel = itemsTable.getSelectionModel();
        
        listSelectionModel.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectedRow = itemsTable.getSelectedRow();
				if (selectedRow != -1) {	
					itemNumField.setText((String) itemsTable.getValueAt(selectedRow, 0));
					itemNameField.setText((String) itemsTable.getValueAt(selectedRow, 1));
					itemStateCombo.setSelectedItem((String) itemsTable.getValueAt(selectedRow, 2));
				}
			}
		});
        
        JScrollPane scrollPane = new JScrollPane(itemsTable);
        scrollPane.setBounds(10, 10, 410, 470);
        
        leftPanel.add(scrollPane);
	}
	
	public boolean checkItemNum(ArrayList<String> itemNum, String inputItemNum) {
		for(int i=0; i<itemNum.size(); i++) {
			if(itemNum.get(i).equals(inputItemNum)) {
				return false;
			} 
		}
		return true;
	}
	
	public boolean nameIsOk(String name) {
		boolean allow = true;
		for(int i=1; i<name.length(); i++) {
			if(name.charAt(i)>='a'&&name.charAt(i)<='z'&&name.charAt(i)<=' '){
			} else {
				allow=false;
				break;
			}
		}
		if(allow)
			return true;
		return false;
	}
	
	public void insertToTable(ArrayList<String> value) {
		int selectedRow = itemsTable.getSelectedRow();
		for (int i = 0; i < 3; i++) {
			itemsTable.setValueAt(value.get(i), selectedRow, i);
		}
		
		setNoCellEditsCount(0);
		clearTextFields();
	}
	
	public void clearTextFields() {
		itemNumField.setText("");
		itemNameField.setText("");
	}
	
	public boolean isempty() {
		if(itemNumField.getText().equals("") && itemNameField.getText().equals(""))
			return true;
		return false;
	}
	
	public ArrayList<String> editItem() {
		int selectedRow = itemsTable.getSelectedRow();
		ArrayList<String> value = new ArrayList<String>();
		
		if (selectedRow != -1) {	
			String itemNum = (String) itemsTable.getValueAt(selectedRow, 0);
			String newItemNum = itemNumField.getText();
			String newItemName = itemNameField.getText();
			String newItemState = (String) itemStateCombo.getSelectedItem();
			
			if (!newItemNum.equals("")) {
				value.add(newItemNum);
			} else {
				value.add((String) itemsTable.getValueAt(selectedRow, 0));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			
			if (!newItemName.equals("")) {
				value.add(newItemName);
			} else {
				value.add((String) itemsTable.getValueAt(selectedRow, 1));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			
			if (!newItemState.equals("")) {
				value.add(newItemState);
			} else {
				value.add((String) itemsTable.getValueAt(selectedRow, 2));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			value.add(itemNum);
		}
			
		return value;
	}

	public void addToTable(String[] itemValues) {
		Object[][] temp = new Object[tableData.length+1][5];
		
		for (int i = 0; i < tableData.length; i++) {
			for (int j = 0; j < 5; j++) {
				temp[i][j] = tableData[i][j];
			}
		}
		
		temp[tableData.length][0] = itemValues[0];
		temp[tableData.length][1] = itemValues[1];
		temp[tableData.length][2] = itemValues[2];
		
		tableData = temp;
		itemsTable.setModel(new MyTableModel(tableData, columnNames));
		
		clearTextFields();
	}
	
	public String[] addItem() {
		String[] itemValues = new String[3];
		
		if (itemNumField.getText().equals("") || itemNameField.getText().equals("")
			|| itemStateCombo.getSelectedItem().equals("")) {
			return null;
		} else {
			itemValues[0] = itemNumField.getText();
			itemValues[1] = itemNameField.getText();
			itemValues[2] = (String) itemStateCombo.getSelectedItem();
			
			return itemValues;
		}
	}
	
	public String[] addItem(String itemNumber, String itemName) {
		Object[][] temp = new Object[tableData.length+1][5];
		String[] item = new String[2];
		
		for (int i = 0; i < tableData.length; i++) {
			for (int j = 0; j < 5; j++) {
				temp[i][j] = tableData[i][j];
			}
		}
		
		temp[tableData.length][0] = item[0] = itemNumber;
		temp[tableData.length][1] = item[1] = itemName;
		temp[tableData.length][2] = "intact";
		
		tableData = temp;
		itemsTable.setModel(new MyTableModel(tableData, columnNames));
		return item;
	}
	
	public ArrayList<String> deleteItem() {
		int[] selectedItems = itemsTable.getSelectedRows();
		Object[][] temp = new Object[tableData.length-selectedItems.length][5];
		ArrayList<String> itemNums = new ArrayList<String>();
		int row = 0;
		
		if (selectedItems.length < 1) {
			return null;
		}
		
		for (int i = 0; i < tableData.length; i++) {
			if (isIn(selectedItems, i) == false) {
				temp[row][0] = tableData[i][0];
				temp[row][1] = tableData[i][1];
				temp[row][2] = tableData[i][2];
				temp[row][3] = tableData[i][3];
				temp[row][4] = tableData[i][4];
				row++;
				//System.out.println(temp[row][0]+", "+temp[row][1]+", "
				//		+temp[row][2]+", "+temp[row][3]+", "+temp[row][4]);
			} else {
				itemNums.add((String) tableData[i][0]);
			}
				
		}
		
		tableData = temp;
		itemsTable.setModel(new MyTableModel(tableData, columnNames));
		return itemNums;
	}
	
	private boolean isIn(int[] selectedItems, int item) {
		
		for (int i = 0; i < selectedItems.length; i++) {
			if (selectedItems[i] == item)
				return true;
		}
		
		return false;
	}
	
	public JButton getEditButton() {
		return editButton;
	}
	
	public JButton getDeleteButton() {
		return deleteButton;
	}
	
	public JButton getNewItemButton() {
		return newButton;
	}
	
	class MyTableModel extends AbstractTableModel {
        private String[] columnNames;
        private Object[][] data;

        public MyTableModel(Object[][] data, String[] columnNames) {
        	this.data = data;
        	this.columnNames = columnNames;
        }
        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        public boolean isCellEditable(int row, int col) {
            if (row < data.length) {
                return false;
            } else {
                return true;
            }
        }

        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }
	
	public String[][] initData(ArrayList<String> itemInfos) {
		String[][] data = new String[itemInfos.size()][5];
		for (int i = 0; i < itemInfos.size(); i++) {
			String[] itemDatas = itemInfos.get(i).split(",");
			data[i][0] = itemDatas[0];
			data[i][1] = itemDatas[1];
			data[i][2] = itemDatas[2];
		}
		
		return data;
	}

	public ItemsHistoryPanel getItemsHistoryPanel() {
		return itemsHistoryPanel;
	}

	public void setItemsHistoryPanel(ItemsHistoryPanel itemsHistoryPanel) {
		this.itemsHistoryPanel = itemsHistoryPanel;
	}

	public int getNoCellEditsCount() {
		return noCellEditsCount;
	}

	public void setNoCellEditsCount(int noCellEditsCount) {
		this.noCellEditsCount = noCellEditsCount;
	}
}
