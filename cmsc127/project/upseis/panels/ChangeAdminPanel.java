package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import cmsc127.project.upseis.infoclasses.AdministratorInfo;

public class ChangeAdminPanel extends JPanel {

	private JLabel nameLabel;
	private JLabel empFirstNameLabel;
	private JLabel empLastNameLabel;
	private JLabel empNumLabel;
	private JLabel empPassLabel;
	private JLabel retypePassLabel;
	
	private JTextField empFirstNameField;
	private JTextField empLastNameField;
	private JTextField empNumField;

	private JPasswordField empPassField;
	private JPasswordField retypePassField;	
	
	private JButton createAccButton;
	private JButton cancelButton;
	
	public ChangeAdminPanel() {
		
		initComponents();
	}
	
	public void initComponents() {
		removeAll();
		setBackground(new Color(236, 233, 197));
		setLayout(null);

		Image img;
		Font txtFont = new Font("Sans", 0, 17);
		Color textColor = new Color(56, 43, 38);
		Color bg = new Color(174, 232, 237);
		
		nameLabel = new JLabel();
		nameLabel.setBounds(355, 90, 150, 22);
		try {
			img = ImageIO.read(getClass().getResource("/namelabel.png"));
			nameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		empFirstNameLabel = new JLabel();
		empFirstNameLabel.setBounds(405, 140, 150, 22);
		try {
			img = ImageIO.read(getClass().getResource("/firstnamelabel.png"));
			empFirstNameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setEmpFirstNameField(new JTextField(30));
		getEmpFirstNameField().setForeground(textColor);
		getEmpFirstNameField().setFont(txtFont);
		getEmpFirstNameField().setBackground(bg);
		getEmpFirstNameField().setBounds(355, 112, 130, 30);
		
		empLastNameLabel = new JLabel();
		empLastNameLabel.setBounds(539, 140, 150, 22);
		try {
			img = ImageIO.read(getClass().getResource("/lastnamelabel.png"));
			empLastNameLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		setEmpLastNameField(new JTextField(30));
		getEmpLastNameField().setForeground(textColor);
		getEmpLastNameField().setFont(txtFont);
		getEmpLastNameField().setBackground(bg);
		getEmpLastNameField().setBounds(489, 112, 130,30);
		
		empNumLabel = new JLabel();
		empNumLabel.setBounds(355, 162, 180, 22);
		try {
			img = ImageIO.read(getClass().getResource("/changeadempnumlabel.png"));
			empNumLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		setEmpNumField(new JTextField(30));
		getEmpNumField().setForeground(textColor);
		getEmpNumField().setFont(txtFont);
		getEmpNumField().setBackground(bg);
		getEmpNumField().setBounds(355, 184, 264, 30);
		
		empPassLabel = new JLabel();
		empPassLabel.setBounds(355, 222, 150, 22);
		try {
			img = ImageIO.read(getClass().getResource("/changeadpasslabel.png"));
			empPassLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		setEmpPassField(new JPasswordField(30));
		getEmpPassField().setForeground(textColor);
		getEmpPassField().setFont(txtFont);
		getEmpPassField().setBackground(bg);
		getEmpPassField().setBounds(355, 244, 264, 30);
		
		retypePassLabel = new JLabel("Retype Password");
		retypePassLabel.setBounds(355, 282, 180, 22);
		try {
			img = ImageIO.read(getClass().getResource("/changeadretypepasslabel.png"));
			retypePassLabel.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		setRetypePassField(new JPasswordField(30));
		getRetypePassField().setForeground(textColor);
		getRetypePassField().setFont(txtFont);
		getRetypePassField().setBackground(bg);
		getRetypePassField().setBounds(355, 304, 264, 30);
		
		setCreateAccButton(new JButton());
		getCreateAccButton().setBounds(319, 410, 150, 40);
		try {
			img = ImageIO.read(getClass().getResource("/createacc.png"));
			getCreateAccButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/createaccclicked.png"));
			getCreateAccButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		setCancelButton(new JButton());
		getCancelButton().setBounds(505, 410, 150, 40);
		try {
			img = ImageIO.read(getClass().getResource("/cancel.png"));
			getCancelButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/cancelclicked.png"));
			getCancelButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		add(nameLabel);
		add(empFirstNameLabel);
		add(empFirstNameField);
		add(empLastNameLabel);
		add(empLastNameField);
		add(empNumLabel);
		add(empNumField);
		add(empPassLabel);
		add(empPassField);
		add(retypePassLabel);
		add(retypePassField);
		add(createAccButton);
		add(cancelButton);
	}

	public void clearComponents() {
		empFirstNameField.setText("");
		empLastNameField.setText("");
		empNumField.setText("");
		empPassField.setText("");
		retypePassField.setText("");
	}
	
	public JTextField getEmpFirstNameField() {
		return empFirstNameField;
	}

	public void setEmpFirstNameField(JTextField empFirstNameField) {
		this.empFirstNameField = empFirstNameField;
	}

	public JTextField getEmpLastNameField() {
		return empLastNameField;
	}

	public void setEmpLastNameField(JTextField empLastNameField) {
		this.empLastNameField = empLastNameField;
	}

	public JTextField getEmpNumField() {
		return empNumField;
	}

	public void setEmpNumField(JTextField empNumField) {
		this.empNumField = empNumField;
	}

	public JPasswordField getEmpPassField() {
		return empPassField;
	}

	public void setEmpPassField(JPasswordField empPassField) {
		this.empPassField = empPassField;
	}
	
	public JPasswordField getRetypePassField() {
		return retypePassField;
	}

	public void setRetypePassField(JPasswordField retypePassField) {
		this.retypePassField = retypePassField;
	}
	
	public JButton getCreateAccButton() {
		return createAccButton;
	}

	public void setCreateAccButton(JButton createAccButton) {
		this.createAccButton = createAccButton;
	}
	
	public JButton getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(JButton cancelButton) {
		this.cancelButton = cancelButton;
	}
	
	public boolean checkEmpNum(String empNum) {
		boolean allow = true;
		if(empNum.length()<=30) {
			for(int i=0; i<empNum.length(); i++) {
				if(empNum.charAt(i) >= '0' && empNum.charAt(i) <= '9') {
				} else {
					allow = false;
				}
			}
			if(allow) 
				return true;
		}
		return false;
	}
	
	public boolean checkEqual(String password, String rePassword) {
		if(password.equals(rePassword))
			return true;
		else 
			return false;
	}
	
	public boolean nameIsOk(String name) {
		boolean allow = true;
		if((name.charAt(0)<='Z'&&name.charAt(0)>='A')) {
			for(int i=1; i<name.length(); i++) {
				if(name.charAt(i)>='a'&&name.charAt(i)<='z'){
				} else {
					allow=false;
					break;
				}
			}
			if(allow)
				return true;
		}
		return false;
	}
	
	public boolean checkExistingAdmin(ArrayList<String> empNumbers, String empNumber) {
		for(int i=0; i<empNumbers.size(); i++) {
			if(empNumbers.get(i).equals(empNumber)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isComplete(String empFirst, String empLast, String empNum, String password, String rePassword) {
		if(empFirst.equals("") || empLast.equals("") || empNum.equals("") || password.equals("") || rePassword.equals("")) {
			return false;
		} else 
			return true;
	}
	
	public AdministratorInfo getAdministratorInfo() {
		AdministratorInfo administratorInfo = new AdministratorInfo();
		administratorInfo.setEmpFirstName(getEmpFirstNameField().getText());
		administratorInfo.setEmpLastName(getEmpLastNameField().getText());
		administratorInfo.setEmpNumber(getEmpNumField().getText());
		administratorInfo.setEmpPassword(new String(getEmpPassField().getPassword()));
		return administratorInfo;
	}
}
