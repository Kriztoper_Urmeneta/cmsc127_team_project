package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Font;
import javax.swing.JDialog;

public class DialogPanel extends JDialog implements ActionListener {

	private JLabel msgLabel;
	
	public DialogPanel() {
		setLayout(null);
		this.getContentPane().setBackground(new Color(236, 233, 197));
		setResizable(false);
		setSize(320, 130);
		setLocation(300, 300);
		setAlwaysOnTop(true);
		
		JPanel messagePane = new JPanel();
		messagePane.setBackground(new Color(236, 233, 197));
		msgLabel = new JLabel("");
		msgLabel.setFont(new Font("", Font.BOLD, 16));
		messagePane.add(msgLabel);
		messagePane.setBounds(0, 10, 300, 50);
		getContentPane().add(messagePane);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setBackground(new Color(236, 233, 197));
		buttonPane.setLayout(null);
		buttonPane.setBounds(0, 50, 300, 50);
		JButton button = new JButton(); 
    
		Image img;
		button.setBounds(170, 10, 122, 28);
		try {
			img = ImageIO.read(getClass().getResource("/ok.png"));
			button.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/okclicked.png"));
			button.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		buttonPane.add(button); 
		button.addActionListener(this);
		getContentPane().add(buttonPane);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
	public void showDialog(JFrame parent, String title, String message) {
		setTitle(title);
		setLocationRelativeTo(null);
		//setLocationRelativeTo(parent);
		msgLabel.setText(message);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		setVisible(false); 
		dispose(); 
	}
}
