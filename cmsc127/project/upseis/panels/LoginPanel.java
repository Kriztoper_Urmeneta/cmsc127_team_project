package cmsc127.project.upseis.panels;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

public class LoginPanel extends JPanel {
	
	private JLabel empNumberLabel;
	private JLabel empPasswordLabel;
	private JLabel titleLabel;
	private JLabel subtitleLabel;
	
	private JTextField empNumberField;
	
	private JPasswordField empPasswordField;
	
	private JButton loginButton;
	private JButton changeAdminButton;
	
	public LoginPanel() {
		initPanel();
	}
	
	public void initPanel() {
		setLayout(null);
		setBackground(new Color(236, 233, 197));
		initComponents();
	}
	
	public void initComponents() {
		removeAll();
		Font txtFont = new Font("Sans", 0, 18);
		Color textColor = new Color(56, 43, 38);
		Image img;
	
		titleLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/upseis.png"));
			titleLabel.setIcon(new ImageIcon(img));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		titleLabel.setBounds(285, 59, 409, 122);
		
		subtitleLabel = new JLabel();
		try {
			img = ImageIO.read(getClass().getResource("/sublabel.png"));
			subtitleLabel.setIcon(new ImageIcon(img));
		} catch (IOException e1) {
			e1.printStackTrace();
		}		
		subtitleLabel.setBounds(291, 163, 401, 22);
		
		empNumberLabel = new JLabel("Employee Number");
		empNumberLabel.setForeground(textColor);
		empNumberLabel.setFont(txtFont);
		empNumberLabel.setBounds(290, 251, 167, 22);
		
		empPasswordLabel = new JLabel("Password");
		empPasswordLabel.setForeground(textColor);
		empPasswordLabel.setFont(txtFont);
		empPasswordLabel.setBounds(290, 314, 85, 22);
		
		empNumberField = new JTextField(30);
		empNumberField.setForeground(textColor);
		empNumberField.setFont(txtFont);
		empNumberField.setBackground(new Color(174, 232, 237));
		empNumberField.setBounds(290, 275, 400, 30);
		
		empPasswordField = new JPasswordField(30);
		empPasswordField.setForeground(textColor);
		empPasswordField.setFont(txtFont);
		empPasswordField.setBackground(new Color(174, 232, 237));
		empPasswordField.setBounds(290, 338, 400, 30);
		
		changeAdminButton = new JButton();
		changeAdminButton.setBounds(290, 390, 190, 40);
	
		try {
			img = ImageIO.read(getClass().getResource("/changeadmin.png"));
			changeAdminButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/changeadminhover.png"));
			changeAdminButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		loginButton = new JButton();
		loginButton.setBounds(500, 390, 190, 40);
		try {
			img = ImageIO.read(getClass().getResource("/login.png"));
			loginButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/loginhover.png"));
			loginButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		add(titleLabel);
		add(subtitleLabel);
		add(empNumberLabel);
		add(empNumberField);
		add(empPasswordLabel);
		add(empPasswordField);
		add(changeAdminButton);
		add(loginButton);
	}

	public void clearComponents() {
		empNumberField.setText("");
		empPasswordField.setText("");
	}
	
	public JButton getLoginButton() {
		return loginButton;
	}

	public void setLoginButton(JButton loginButton) {
		this.loginButton = loginButton;
	}

	public JTextField getEmpNumberField() {
		return empNumberField;
	}

	public void setEmpNumberField(JTextField empNumberField) {
		this.empNumberField = empNumberField;
	}

	public JPasswordField getEmpPasswordField() {
		return empPasswordField;
	}

	public void setEmpPasswordField(JPasswordField empPasswordField) {
		this.empPasswordField = empPasswordField;
	}

	public JButton getChangeAdminButton() {
		return changeAdminButton;
	}

	public void setChangeAdminButton(JButton changeAdminButton) {
		this.changeAdminButton = changeAdminButton;
	}
	
	/*public boolean empNumberFieldIsOk() {
		
	}*/
}
