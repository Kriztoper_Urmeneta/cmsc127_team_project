package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.print.attribute.standard.RequestingUserName;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import cmsc127.project.upseis.panels.ViewItemsFrame.MyTableModel;

public class BorrowersListPanel extends JPanel {

	private int noCellEditsCount;
	
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JLabel studentNumLabel;
	private JLabel contactNumLabel;
	private JLabel teamNameLabel;
	
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField studentNumField;
	private JTextField contactNumField;
	private JTextField teamNameField;
	
	private JButton addBorrowerButton;
	private JButton editBorrowerButton;
	private JButton deleteBorrowersButton;
	private JButton borrowersHistoryButton;
	private DefaultTableModel borrowersModel;
	private JTable borrowersTable;
	private JScrollPane borrowersTablePane;
	
	public BorrowersListPanel() {
		initPanel();
		
		initComponents();
		setNoCellEditsCount(0);
	}
	
	public void initPanel() {
		setLayout(null);
		setBackground(new Color(236, 233, 197));
	}
	
	public void initComponents() {
		Image img;
		
		setAddBorrowerButton(new JButton());
		getAddBorrowerButton().setBounds(20, 495, 160, 40);
		
		try {
			img = ImageIO.read(getClass().getResource("/add.png"));
			getAddBorrowerButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/addclicked.png"));
			getAddBorrowerButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		editBorrowerButton = new JButton();
		editBorrowerButton.setBounds(200, 495, 160, 40);
		try {
			img = ImageIO.read(getClass().getResource("/edit.png"));
			editBorrowerButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/editclicked.png"));
			editBorrowerButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		deleteBorrowersButton = new JButton();
		deleteBorrowersButton.setBounds(380, 495, 160, 40);
		try {
			img = ImageIO.read(getClass().getResource("/delete.png"));
			deleteBorrowersButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/deleteclicked.png"));
			deleteBorrowersButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setBorrowersHistoryButton(new JButton());
		getBorrowersHistoryButton().setBounds(590, 495, 160, 40);
		try {
			img = ImageIO.read(getClass().getResource("/borrowerhistory.png"));
			getBorrowersHistoryButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/borrowerhistoryclicked.png"));
			getBorrowersHistoryButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] itemsColumnNames = {"First Name","Last Name","Student Number",
			"Contact Number","Team Name"};
		borrowersModel = new DefaultTableModel(itemsColumnNames, 0) {
			 public boolean isCellEditable(int row, int column)
			 {
			     return false;
			 }
		};
		borrowersTable = new JTable(borrowersModel);
		borrowersTable.setBackground(new Color(174, 232, 237));
		borrowersTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		borrowersTablePane = new JScrollPane(borrowersTable);
		borrowersTablePane.setBounds(20, 10, 730, 420);
		borrowersTable.setPreferredScrollableViewportSize(new Dimension(440, 100));
		
		ListSelectionModel listSelectionModel = borrowersTable.getSelectionModel();
        
        listSelectionModel.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				int selectedRow = borrowersTable.getSelectedRow();
				if (selectedRow != -1) {	
					firstNameField.setText((String) borrowersTable.getValueAt(selectedRow, 0));
					lastNameField.setText((String) borrowersTable.getValueAt(selectedRow, 1));
					studentNumField.setText((String) borrowersTable.getValueAt(selectedRow, 2));
					contactNumField.setText((String) borrowersTable.getValueAt(selectedRow, 3));
					teamNameField.setText((String) borrowersTable.getValueAt(selectedRow, 4));
				}
			}
		});
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		firstNameLabel = new JLabel("First Name");
		firstNameLabel.setBounds(20, 435, 142, 20);
		
		lastNameLabel = new JLabel("Last Name");
		lastNameLabel.setBounds(168, 435, 140, 20);
		
		studentNumLabel = new JLabel("Student Number");
		studentNumLabel.setBounds(314, 435, 140, 20);
		
		contactNumLabel = new JLabel("Contact Number");
		contactNumLabel.setBounds(460, 435, 140, 20);
		
		teamNameLabel = new JLabel("Team Name");
		teamNameLabel.setBounds(606, 435, 143, 20);
		
		firstNameField = new JTextField(30);
		firstNameField.setBackground(new Color(174, 232, 237));
		firstNameField.setBounds(20, 455, 142, 25);
		
		lastNameField = new JTextField(30);
		lastNameField.setBackground(new Color(174, 232, 237));
		lastNameField.setBounds(168, 455, 140, 25);
		
		setStudentNumField(new JTextField(30));
		getStudentNumField().setBackground(new Color(174, 232, 237));
		getStudentNumField().setBounds(314, 455, 140, 25);
		
		contactNumField = new JTextField(30);
		contactNumField.setBackground(new Color(174, 232, 237));
		contactNumField.setBounds(460, 455, 140, 25);
		
		teamNameField = new JTextField(30);
		teamNameField.setBackground(new Color(174, 232, 237));
		teamNameField.setBounds(606, 455, 143, 25);
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		add(firstNameLabel);
		add(lastNameLabel);
		add(studentNumLabel);
		add(contactNumLabel);
		add(teamNameLabel);
		add(firstNameField);
		add(lastNameField);
		add(getStudentNumField());
		add(contactNumField);
		add(teamNameField);
		add(borrowersTablePane);
		add(getAddBorrowerButton());
		add(editBorrowerButton);
		add(deleteBorrowersButton);
		add(getBorrowersHistoryButton());
	}
	
	public ArrayList<String> deleteBorrowers() {
		int[] selectedRows = borrowersTable.getSelectedRows();
		ArrayList<String> studentNums = new ArrayList<String>();
		
		if (selectedRows.length == 0) {
			setNoCellEditsCount(5);
			//JOptionPane.showMessageDialog(this, "You must select row(s) to delete.");
		}
		
		for (int i = selectedRows.length-1; i >= 0; i--) {
			studentNums.add((String) borrowersModel.getValueAt(selectedRows[i], 2));
			borrowersModel.removeRow(selectedRows[i]);
		}
		return studentNums;
	}
	
	public void insertToTable(ArrayList<String> value) {
		int selectedRow = borrowersTable.getSelectedRow();
		for (int i = 0; i < 5; i++) {
			borrowersTable.setValueAt(value.get(i), selectedRow, i);
		}
		
		setNoCellEditsCount(0);
		clearTextFields();
	}
	
	public ArrayList<String> editBorrower() {
		int selectedRow = borrowersTable.getSelectedRow();
		ArrayList<String> value = new ArrayList<String>();
		if (selectedRow != -1) {
			String studentNum = (String) borrowersTable.getValueAt(selectedRow, 2);
			String newFirstName = firstNameField.getText();
			String newLastName = lastNameField.getText();
			String newStudentNum = getStudentNumField().getText();
			String newContactNum = contactNumField.getText();
			String newTeamName = teamNameField.getText();
			
			if (!newFirstName.equals("")) {
				value.add(newFirstName);
			} else {
				value.add((String) borrowersTable.getValueAt(selectedRow, 0));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			
			if (!newLastName.equals("")) {
				value.add(newLastName);
			} else {
				value.add((String) borrowersTable.getValueAt(selectedRow, 1));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			
			if (!newStudentNum.equals("")) {
				value.add(newStudentNum);
			} else {
				value.add((String) borrowersTable.getValueAt(selectedRow, 2));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			
			if (!newContactNum.equals("")) {
				value.add(newContactNum);
			} else {
				value.add((String) borrowersTable.getValueAt(selectedRow, 3));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			
			if (!newTeamName.equals("")) {
				value.add(newTeamName);
			} else {
				value.add(((String) borrowersTable.getValueAt(selectedRow, 4))
					.replaceAll("\n", ""));
				setNoCellEditsCount(getNoCellEditsCount() + 1);
			}
			value.add(studentNum);
		}	
		return value;
	}
	
	public void clearTextFields() {
		firstNameField.setText("");
		lastNameField.setText("");
		getStudentNumField().setText("");
		contactNumField.setText("");
		teamNameField.setText("");
	}
	
	public boolean checkTeamName(String teamName) {
		boolean allow = true;
			for(int i=1; i<teamName.length(); i++) {
				if((teamName.charAt(i)>='a'&&teamName.charAt(i)<='z') || (teamName.charAt(i)>='A'&&teamName.charAt(i)<='Z')){
				} else {
					allow=false;
					break;
				}
			}
			if(allow)
				return true;
		return false;
	}
	
	public boolean nameIsOk(String name) {
		boolean allow = true;
		if((name.charAt(0)<='Z'&&name.charAt(0)>='A')) {
			for(int i=1; i<name.length(); i++) {
				if(name.charAt(i)>='a'&&name.charAt(i)<='z'){
				} else {
					allow=false;
					break;
				}
			}
			if(allow)
				return true;
		}
		return false;
	}
	
	public boolean checkContNum(String contactNum) {
		boolean pwede = true;
		if(contactNum.length()==11 && contactNum.charAt(0)=='0' && contactNum.charAt(1)=='9') {
			for(int i=0; i<contactNum.length(); i++) {
				if(contactNum.charAt(i) >= '0' && contactNum.charAt(i) <= '9') {
				} else {
					pwede = false;
				}
			}
			if(pwede) 
				return true;
		}
		return false;
	}
	public boolean checkStudNum(String string) {
		boolean pwede = true;
		if(string.length()==9) {
			for(int i=0; i<string.length(); i++) {
				if(string.charAt(i) >= '0' && string.charAt(i) <= '9') {
				} else {
					pwede = false;
				}
			}
			if(pwede) 
				return true;
		}
		return false;
	}
	
	public boolean checkStudentNum(ArrayList<String> studentNum, String inputStudentNum) {
		int selectedRow = borrowersTable.getSelectedRow();
		for(int i=0; i<studentNum.size(); i++) {
			if(studentNum.get(i).equals(inputStudentNum) 
					&& !borrowersTable.getValueAt(selectedRow, 2).equals(inputStudentNum)) {
				return false;
			} 
		}
		return true;
	}
	
	public boolean isempty() {
		if(firstNameField.getText().equals("") && lastNameField.getText().equals("") 
				&& getStudentNumField().getText().equals("") && contactNumField.getText().equals("") 
				&& teamNameField.getText().equals(""))
			return true;
		return false;
	}
	
	public void setItemsTable(ArrayList<String> itemInfos, String[] columnNames) {
		borrowersModel.setRowCount(0);
		for (String x: itemInfos) {
			borrowersModel.addRow(x.split(","));
		}
	}
	
	public String[][] initData(ArrayList<String> itemInfos) {
		String[][] data = new String[itemInfos.size()][5];
		for (int i = 0; i < itemInfos.size(); i++) {
			String[] itemDatas = itemInfos.get(i).split(",");
			data[i][0] = itemDatas[0];
			data[i][1] = itemDatas[1];
			data[i][2] = itemDatas[2];
			data[i][3] = itemDatas[3];
			data[i][4] = itemDatas[4];
		}
		
		return data;
	}

	public JButton getDeleteBorrowersButton() {
		return deleteBorrowersButton;
	}

	public void setDeleteBorrowersButton(JButton deleteBorrowersButton) {
		this.deleteBorrowersButton = deleteBorrowersButton;
	}

	public JButton getEditBorrowerButton() {
		return editBorrowerButton;
	}

	public void setEditBorrowerButton(JButton editBorrowerButton) {
		this.editBorrowerButton = editBorrowerButton;
	}

	public JButton getAddBorrowerButton() {
		return addBorrowerButton;
	}

	public void setAddBorrowerButton(JButton addBorrowerButton) {
		this.addBorrowerButton = addBorrowerButton;
	}

	public JButton getBorrowersHistoryButton() {
		return borrowersHistoryButton;
	}

	public void setBorrowersHistoryButton(JButton borrowersHistoryButton) {
		this.borrowersHistoryButton = borrowersHistoryButton;
	}

	public int getNoCellEditsCount() {
		return noCellEditsCount;
	}

	public void setNoCellEditsCount(int noCellEditsCount) {
		this.noCellEditsCount = noCellEditsCount;
	}

	public JTextField getStudentNumField() {
		return studentNumField;
	}

	public void setStudentNumField(JTextField studentNumField) {
		this.studentNumField = studentNumField;
	}
}
