package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class BorrowersHistoryPanel extends JPanel {

	private JLabel note;
	private JLabel searchLabel;
	private JTextField searchField;
	private JButton searchButton;
	private JButton backButton;
	private DefaultTableModel model;
	private JTable borrowersTable;
	private JScrollPane itemsTablePane;
	String[] columnNames = {"Borrower Name","Item Name","Item Number", "State", "Date Borrowed", "Date Returned"};
	
	public BorrowersHistoryPanel() {
		initPanel();

		initComponents();
		addSearchBar();
	}
	
	public void initPanel() {
		setBackground(new Color(236, 233, 197));
		setLayout(null);
	}
	
	public void initComponents() {
		Image img;
		
		searchLabel = new JLabel("Enter student number");
		searchLabel.setBounds(20, 1, 300, 22);
		
		searchField = new JTextField(20);
		searchField.setForeground(new Color(56, 43, 38));
		searchField.setFont(new Font("Sans", 0, 17));
		searchField.setBackground(new Color(174, 232, 237));
		searchField.setBounds(20, 20, 300, 25);
		
		searchButton = new JButton();
		searchButton.setBounds(320, 20, 95, 25);
		try {
			img = ImageIO.read(getClass().getResource("/search.png"));
			searchButton.setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/searchclicked.png"));
			searchButton.setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setBackButton(new JButton());
		try {
			img = ImageIO.read(getClass().getResource("/back.png"));
			getBackButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/backclicked.png"));
			getBackButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		getBackButton().setBounds(655, 20, 95, 25);
		
		model = new DefaultTableModel(columnNames, 0) {
			 public boolean isCellEditable(int row, int column)
			 {
			     return false;
			 }
		};
		borrowersTable = new JTable(model);
		borrowersTable.setAutoCreateRowSorter(true);
		borrowersTable.setBackground(new Color(174, 232, 237));
		borrowersTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		borrowersTable.setPreferredScrollableViewportSize(new Dimension(730, 470));
		
		itemsTablePane = new JScrollPane(borrowersTable);
		itemsTablePane.setBounds(20, 50, 730, 470);
		
		add(itemsTablePane);
		
		note = new JLabel("<html>Note: Resize columns by dragging to get a full view on table values."
				+ "<br />Click on column title to sort records.</html>");
		note.setBounds(265, 520, 740, 40);
		add(note);
	}
	
	public void setItemsTable() {
		searchField.setText("");
		model.setRowCount(0);
	}
	
	public void setItemsTable(ArrayList<String> itemsInfo) {
		model.setRowCount(0);
		for (String x: itemsInfo) {
			String[] attributes = x.split(",");
			model.addRow(attributes);
		}
	}
	
	public void addSearchBar() {
		
		add(searchLabel);
		add(searchField);
		add(searchButton);
		add(getBackButton());
	}
	
	public String[][] initData(ArrayList<String> itemInfos) {
		String[][] data = new String[itemInfos.size()][7];
		for (int i = 0; i < itemInfos.size(); i++) {
			String[] itemDatas = itemInfos.get(i).split(",");
			data[i][0] = itemDatas[0];
			data[i][1] = itemDatas[1];
			data[i][2] = itemDatas[2];
			data[i][3] = itemDatas[3];
			data[i][4] = itemDatas[4];
			data[i][5] = itemDatas[5];
		}
		
		return data;
	}
	
	public JButton getSearchButton() {
		return searchButton;
	}
	
	public JTextField getSearchField() {
		return searchField;
	}

	public JButton getBackButton() {
		return backButton;
	}

	public void setBackButton(JButton backButton) {
		this.backButton = backButton;
	}
}