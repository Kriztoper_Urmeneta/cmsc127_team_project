package cmsc127.project.upseis.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import cmsc127.project.upseis.infoclasses.ItemInfo;

public class ManageItemsPanel extends JPanel {

	private final String[] itemsColumnNames = {"Item Number","Item Name","State","Status"};
	private final String[] borrowersColumnNames = {"First Name","Last Name","Student No.",
			"Contact #", "Team Name"};
	private final String[] borrowedItemsColumnNames = {"Item Number","Item Name","State"};
	
	private ArrayList<String> itemsStatus;
	
	private JPanel displayPane;

	private JLabel searchItemsLabel;
	private JTextField searchItemsField;
	private JButton searchButton;
	private JTable itemsTable;
	
	private ButtonGroup radioGroup;
	private JRadioButton borrowItemsRadio;
	private JRadioButton reserveItemsRadio;
	
	private JButton borrowItemsButton;
	private JButton returnItemsButton;
	private JButton reserveItemsButton;
	
	private JLabel searchBorrowersLabel;
	private JLabel searchBorrowersLabel2;
	private JTextField searchBorrowersField;
	private JButton searchBorrowersButton;
	private JLabel firstNameLabel;
	private JTextField firstNameField;
	private JLabel lastNameLabel;
	private JTextField lastNameField;
	private JLabel studentNumLabel;
	private JTextField studentNumField;
	private JLabel contactNumLabel;
	private JTextField contactNumField;
	private JLabel teamNameLabel;
	private JTextField teamNameField;

	private JLabel borrowedItemsLabel;
	private JTable borrowedItemsTable;

	private DefaultTableModel itemsModel;
	private DefaultTableModel biModel;
	private JScrollPane itemsTablePane;
	private JScrollPane borrowedItemsTablePane;

	public ManageItemsPanel() {
		initPanel();
		
		initComponents();
		addComponents();
	}
	
	public void initPanel() {
		setBackground(new Color(236, 233, 197));
	}
	
	public void initComponents() {
		Image img;
		displayPane = new JPanel();
		displayPane.setLayout(null);
		displayPane.setBackground(new Color(230, 226, 186));
		displayPane.setBounds(480, 0, 300, 550);
		
		searchItemsLabel = new JLabel("Enter item name to search for items");
		searchItemsLabel.setBounds(20, 10, 270, 20);
		
		setSearchItemsField(new JTextField(30));
		getSearchItemsField().setBackground(new Color(174, 232, 237));
		getSearchItemsField().setBounds(20, 30, 440, 25);
		
		setSearchButton(new JButton());
		getSearchButton().setBounds(330, 60, 130, 25);
		try {
			img = ImageIO.read(getClass().getResource("/managesearch2.png"));
			getSearchButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/managesearchclicked2.png"));
			getSearchButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		itemsModel = new DefaultTableModel(itemsColumnNames, 0) {
			 public boolean isCellEditable(int row, int column)
			 {
			     return false;
			 }
		};
		itemsTable = new JTable(itemsModel);
		itemsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		itemsTable.setPreferredScrollableViewportSize(new Dimension(440, 100));
		itemsTable.setBackground(new Color(174, 232, 237));
		itemsTablePane = new JScrollPane(itemsTable);
		itemsTablePane.setSize(300, 30);
		itemsTablePane.setBounds(20, 90, 440, 415);
		
		borrowItemsRadio = new JRadioButton("Borrow");
		borrowItemsRadio.setBackground(new Color(236, 233, 197));
		borrowItemsRadio.setBounds(130, 60, 100, 30);
		
		reserveItemsRadio = new JRadioButton("Reserve");
		reserveItemsRadio.setBackground(new Color(236, 233, 197));
		reserveItemsRadio.setBounds(230, 60, 100, 30);

		radioGroup = new ButtonGroup();
		radioGroup.add(borrowItemsRadio);
		radioGroup.add(reserveItemsRadio);
		
		setBorrowItemsButton(new JButton());
		getBorrowItemsButton().setBounds(180, 510, 130, 25);
		try {
			img = ImageIO.read(getClass().getResource("/manageborrow.png"));
			getBorrowItemsButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/manageborrowclicked.png"));
			getBorrowItemsButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setReturnItemsButton(new JButton());
		getReturnItemsButton().setBounds(155, 510, 130, 25);
		try {
			img = ImageIO.read(getClass().getResource("/managereturn.png"));
			getReturnItemsButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/managereturnclicked.png"));
			getReturnItemsButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setReserveItemsButton(new JButton());
		getReserveItemsButton().setBounds(330, 510, 130, 25);
		try {
			img = ImageIO.read(getClass().getResource("/managereserve.png"));
			getReserveItemsButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/managereserveclicked.png"));
			getReserveItemsButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		searchBorrowersLabel = new JLabel("Enter student number to search");
		searchBorrowersLabel.setBounds(10, 10, 275, 20);
		searchBorrowersLabel2 = new JLabel("for borrower");
		searchBorrowersLabel2.setBounds(10, 30, 275, 20);
		
		setSearchBorrowersField(new JTextField(30));
		getSearchBorrowersField().setBackground(new Color(174, 232, 237));
		getSearchBorrowersField().setBounds(10, 50, 180, 25);
		
		//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		firstNameLabel = new JLabel("First Name:");
		firstNameLabel.setBounds(10, 80, 200, 20);
		firstNameField = new JTextField(270);
		firstNameField.setEditable(false);
		firstNameField.setBackground(new Color(174, 232, 237));
		firstNameField.setBounds(50, 100, 235, 20);
		lastNameLabel = new JLabel("Last Name:");
		lastNameLabel.setBounds(10, 120, 200, 20);
		lastNameField = new JTextField(270);
		lastNameField.setEditable(false);
		lastNameField.setBackground(new Color(174, 232, 237));
		lastNameField.setBounds(50, 140, 235, 20);
		studentNumLabel = new JLabel("Student Number:");
		studentNumLabel.setBounds(10, 160, 200, 20);
		studentNumField = new JTextField(270);
		studentNumField.setEditable(false);
		studentNumField.setBackground(new Color(174, 232, 237));
		studentNumField.setBounds(50, 180, 235, 20);
		contactNumLabel = new JLabel("Contact Number:");
		contactNumLabel.setBounds(10, 200, 200, 20);
		contactNumField = new JTextField(270);
		contactNumField.setEditable(false);
		contactNumField.setBackground(new Color(174, 232, 237));
		contactNumField.setBounds(50, 220, 235, 20);
		teamNameLabel = new JLabel("Team Name:");
		teamNameLabel.setBounds(10, 240, 200, 20);
		teamNameField = new JTextField(270);
		teamNameField.setEditable(false);
		teamNameField.setBackground(new Color(174, 232, 237));
		teamNameField.setBounds(50, 260, 235, 20);
		//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
		
		setSearchBorrowersButton(new JButton());
		getSearchBorrowersButton().setBounds(190, 50, 95, 25);
		try {
			img = ImageIO.read(getClass().getResource("/search.png"));
			getSearchBorrowersButton().setIcon(new ImageIcon(img));
			img = ImageIO.read(getClass().getResource("/searchclicked.png"));
			getSearchBorrowersButton().setRolloverIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		borrowedItemsLabel = new JLabel("Borrowed items by current borrower");
		borrowedItemsLabel.setBounds(10, 280, 275, 25);
		
		biModel = new DefaultTableModel(borrowedItemsColumnNames, 0) {
			 public boolean isCellEditable(int row, int column)
			 {
			     return false;
			 }
		};
		borrowedItemsTable = new JTable(biModel);
		borrowedItemsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		borrowedItemsTable.setBackground(new Color(174, 232, 237));
		borrowedItemsTable.setPreferredScrollableViewportSize(new Dimension(440, 100));
		borrowedItemsTablePane = new JScrollPane(borrowedItemsTable);
		borrowedItemsTablePane.setBounds(10, 305, 275, 200);
	}
	
	public void addComponents() {
		removeAll();
		setLayout(null);
		
		add(searchItemsLabel);
		add(searchItemsField);
		add(borrowItemsButton);
		add(reserveItemsButton);
		add(itemsTablePane);
		add(searchButton);
		add(borrowItemsRadio);
		add(reserveItemsRadio);

		displayPane.add(searchBorrowersLabel);
		displayPane.add(searchBorrowersLabel2);
		displayPane.add(searchBorrowersField);
		displayPane.add(returnItemsButton);
		displayPane.add(firstNameLabel);
		displayPane.add(firstNameField);
		displayPane.add(lastNameLabel);
		displayPane.add(lastNameField);
		displayPane.add(studentNumLabel);
		displayPane.add(studentNumField);
		displayPane.add(contactNumLabel);
		displayPane.add(contactNumField);
		displayPane.add(teamNameLabel);
		displayPane.add(teamNameField);
		
		displayPane.add(searchBorrowersButton);
		displayPane.add(borrowedItemsLabel);
		displayPane.add(borrowedItemsTablePane);
		add(displayPane);
	}
	
	public void clearComponents() {
		radioGroup.clearSelection();
		searchItemsField.setText("");
		searchBorrowersField.setText("");
		itemsModel.setRowCount(0);
		firstNameField.setText("");
		lastNameField.setText("");
		studentNumField.setText("");
		contactNumField.setText("");
		teamNameField.setText("");
		biModel.setRowCount(0);
	}

	public String getSelectedRadio() {
		if (borrowItemsRadio.isSelected()) {
			return "borrow";
		} else if (reserveItemsRadio.isSelected()) {
			return "reserve";
		}
		
		return "";
	}
	
	public boolean hasItemsSelected() {
		return (itemsTable.getSelectedRow() == -1);
	}
	
	public boolean hasItemsToReturnSelected() {
		return (borrowedItemsTable.getSelectedRow() != -1);
	}
	
	public void setBorrowersTable(ArrayList<String> borrowerInfo) {
		String[] borrowerInfoArray = borrowerInfo.get(0).split(",");
		itemsModel.setRowCount(0);
		firstNameField.setText(borrowerInfoArray[0]);
		lastNameField.setText(borrowerInfoArray[1]);
		studentNumField.setText(borrowerInfoArray[2]);
		contactNumField.setText(borrowerInfoArray[3]);
		teamNameField.setText(borrowerInfoArray[4]);
	}
	
	public void setItemsTable(ArrayList<String> itemsInfo) {
		itemsModel.setRowCount(0);
		for (String x: itemsInfo) {
			String[] attributes = x.split(",");
			String reservationID = attributes[3];
			String borrowerID = attributes[4];
			attributes[3] = getItemStatus(reservationID, borrowerID);
			String[] newAttribs = new String[4];
			for (int i = 0; i < 4; i++) {
				newAttribs[i] = attributes[i];
			}
			itemsModel.addRow(newAttribs);
		}
	}
	
	public boolean itemsTableIsEmpty() {
		return (itemsTable.getSelectedRows().length == 0);
	}
	
	public String getItemStatus(String reservationID, String borrowerID) {
		if (reservationID.equals("null") && borrowerID.equals("null")) {
			return "available";
		} else if (reservationID.equals("null") && !borrowerID.equals("null")) {
			return "borrowed";
		} else if (!reservationID.equals("null") && borrowerID.equals("null")) {
			return "reserved";
		} else if (!reservationID.equals("null") && !borrowerID.equals("null")) {
			return "reserved, borrowed";
		}
		return null;
	}
	
	public void clearItemsTable() {
		itemsModel.setRowCount(0);
		radioGroup.clearSelection();
	}
	
	public void setBorrowedItemsTable(ArrayList<String> itemsInfo) {
		biModel.setRowCount(0);
		for (String x: itemsInfo) {
			biModel.addRow(x.split(","));
		}
	}

	public ArrayList<ItemInfo> getSelectedItemsToReturn() {
		ArrayList<ItemInfo> itemInfos = new ArrayList<ItemInfo>();
		
		int[] rows = borrowedItemsTable.getSelectedRows();

		for (int i = 0; i < rows.length; i++) {
			ItemInfo itemInfo = new ItemInfo();
			itemInfo.setItemNum((String) borrowedItemsTable.getValueAt(rows[i], 0));
			itemInfo.setItemName((String) borrowedItemsTable.getValueAt(rows[i], 1));
			itemInfo.setState((String) borrowedItemsTable.getValueAt(rows[i], 2));
			itemInfos.add(itemInfo);
		}
		
		return itemInfos;		
	}
	
	public ArrayList<ItemInfo> getSelectedItems() {
		ArrayList<ItemInfo> itemInfos = new ArrayList<ItemInfo>();
		
		int[] rows = itemsTable.getSelectedRows();

		for (int i = 0; i < rows.length; i++) {
			ItemInfo itemInfo = new ItemInfo();
			itemInfo.setItemNum((String) itemsTable.getValueAt(rows[i], 0));
			itemInfo.setItemName((String) itemsTable.getValueAt(rows[i], 1));
			itemInfo.setState((String) itemsTable.getValueAt(rows[i], 2));
			itemInfos.add(itemInfo);
		}
		
		return itemInfos;
	}
	
	public void clearTables() {
		itemsModel.setRowCount(0);
		radioGroup.clearSelection();
		firstNameField.setText("");
		lastNameField.setText("");
		studentNumField.setText("");
		contactNumField.setText("");
		teamNameField.setText("");
		biModel.setRowCount(0);
	}
	
	public JTextField getSearchItemsField() {
		return searchItemsField;
	}

	public void setSearchItemsField(JTextField searchItemsField) {
		this.searchItemsField = searchItemsField;
	}

	public JButton getSearchButton() {
		return searchButton;
	}

	public void setSearchButton(JButton searchButton) {
		this.searchButton = searchButton;
	}

	public JButton getBorrowItemsButton() {
		return borrowItemsButton;
	}

	public void setBorrowItemsButton(JButton borrowItemsButton) {
		this.borrowItemsButton = borrowItemsButton;
	}

	public JTextField getSearchBorrowersField() {
		return searchBorrowersField;
	}

	public void setSearchBorrowersField(JTextField searchBorrowersField) {
		this.searchBorrowersField = searchBorrowersField;
	}

	public JButton getSearchBorrowersButton() {
		return searchBorrowersButton;
	}

	public void setSearchBorrowersButton(JButton searchBorrowersButton) {
		this.searchBorrowersButton = searchBorrowersButton;
	}

	public JButton getReturnItemsButton() {
		return returnItemsButton;
	}

	public void setReturnItemsButton(JButton returnItemsButton) {
		this.returnItemsButton = returnItemsButton;
	}

	public JButton getReserveItemsButton() {
		return reserveItemsButton;
	}

	public void setReserveItemsButton(JButton reserveItemsButton) {
		this.reserveItemsButton = reserveItemsButton;
	}
}
