package cmsc127.project.upseis.controller;

import java.util.ArrayList;

import cmsc127.project.upseis.connector.DBConnector;
import cmsc127.project.upseis.infoclasses.AdministratorInfo;
import cmsc127.project.upseis.infoclasses.BorrowerInfo;
import cmsc127.project.upseis.infoclasses.ItemInfo;

public class InventoryController {
	
	private DBConnector dbConnector;
	
	public InventoryController() {
		
		initDBConnector();
	}
	
	public void initDBConnector() {
		dbConnector = new DBConnector();
	}

	public ArrayList <String> viewItems() {
		String sql = "SELECT itemNum, itemName, state FROM items";
		return dbConnector.executeQuery(sql, "view items");
	}
	
	public ArrayList<String> selectItemNum() {
		String sql = "SELECT itemNum from items";
		return dbConnector.executeQuery(sql, "select itemsNum");
	}
	
	public ArrayList<String> selectStudentNum() {
		String sql = "SELECT studentNum from items";
		return dbConnector.executeQuery(sql, "select studentNum");
	}

	public ArrayList<String> selectItemsHistory(String itemName) {
		String sql = "SELECT itemNum, itemName, state, borrowerName, dateBorrowed, "
				+ "dateReturned FROM items_history"
				+ " WHERE itemName LIKE '%"+itemName+"%' OR itemNum LIKE "
				+ "'%"+itemName+"%' ORDER BY itemName";
		return dbConnector.executeQuery(sql, "select items history");
	}
	
	public ArrayList<String> selectBorrowersHistory(String studNum) {
		String sql = "SELECT borrowerName, itemNum, itemName, state, dateBorrowed, dateReturned FROM items_history"
				+ " WHERE borrowerID LIKE '%"+studNum+"%' ORDER BY itemName";
		return dbConnector.executeQuery(sql, "select borrowers history");
	}
	
	public void createBorrower(BorrowerInfo borrowerInfo) {
		String sql = "INSERT INTO borrowers (firstName, lastName, studentNum, contactNum, teamName)"
					+ " VALUES ('"
				+borrowerInfo.getFirstName()+"','"+borrowerInfo.getLastName()
				+"','"+borrowerInfo.getStudentNum()+"','"+borrowerInfo.getContactNum()
				+"',\""+borrowerInfo.getTeamName()+"\")";
		dbConnector.executeUpdate(sql);
	}
	
	public ArrayList<String> studentNumbers() {
		String sql = "SELECT studentNum FROM borrowers;";
		ArrayList<String> borrowerNumbers = dbConnector.executeQuery(sql, "select borrowers number");
		
		return borrowerNumbers;
	}
	
	public void setCurrentAdminToInactive() {
		dbConnector.executeUpdate("UPDATE administrators "
								+ "SET activity='inactive'"
								+ "WHERE activity='active'");
	}
	
	public void insertAdministrator(AdministratorInfo administratorInfo) {
		String sql = "INSERT INTO administrators (empFirstName, empLastName, empNumber, empPassword)"
					+ " VALUES ('"
				+administratorInfo.getEmpFirstName()+"','"+administratorInfo.getEmpLastName()
				+"','"+administratorInfo.getEmpNumber()+"','"+administratorInfo.getEmpPassword()+"')";
		dbConnector.executeUpdate(sql);
	}

	public ArrayList<String> selectBorrowersList() {
		String sql = "SELECT * FROM borrowers";
		ArrayList<String> itemsList = dbConnector.executeQuery(sql, "select borrowers list");
		for (int i = 0; i < itemsList.size(); i++) {
			itemsList.set(i, itemsList.get(i)+"\n");
		}
		return itemsList;
	}
	
	public ArrayList<String> getCurrentAdminNumber() {
		String sql = "SELECT empNumber FROM administrators WHERE activity='active'";
		ArrayList<String> strings = (dbConnector.executeQuery(sql, "select current employee number"));
		return strings;
	}
	
	public ArrayList<String> getCurrentAdminPassword() {
		String sql = "SELECT empPassword FROM administrators WHERE activity='active'";
		ArrayList<String> strings = (dbConnector.executeQuery(sql, "select current employee password"));
		return strings;
	}
	
	public ArrayList<String> getAdministratorsEmpNum() {
		String sql = "SELECT empNumber FROM administrators;";
		ArrayList<String> strings = (dbConnector.executeQuery(sql, "select current employee number"));
		return strings;
	}
	
	public ArrayList<String> getSearchedItems(String itemName) { 
		String sql = "SELECT * FROM items "
			+ "WHERE itemName LIKE '%"+itemName+"%'";
		return (dbConnector.executeQuery(sql, "select available items"));
	}
	
	public ArrayList<String> getAvailableItems(String itemName) {
		String sql = "SELECT * FROM items "
			+ "WHERE reservationID IS NULL AND borrowerID IS NULL "
			+ "AND itemName LIKE '%"+itemName+"%'";
		ArrayList<String> strings = (dbConnector.executeQuery(sql, "select available items"));
		return strings;
	}
	
	public ArrayList<String> getAvailableOrReservedItems(String itemName, String studentNum) {
		String sql = "SELECT * FROM items "
			+ "WHERE ((reservationID IS NULL AND borrowerID IS NULL) "
			+ "OR reservationID='"+studentNum+"')"
			+ "AND itemName LIKE '%"+itemName+"%'";
		ArrayList<String> strings = (dbConnector.executeQuery(sql, "select available items"));
		return strings;
	}
	
	public void borrowSelectedItems(ArrayList<ItemInfo> itemInfos, String studentNum) {
		ArrayList<String> itemNumsList = new ArrayList<String>();
		for (int i = 0; i < itemInfos.size(); i++) {
			itemNumsList.add("\""+itemInfos.get(i).getItemNum()+"\"");
		}
		String itemNums = itemNumsList.toString().replace("[", "(").replace("]", ")")
			.replace(", ", ",");
		dbConnector.executeUpdate("UPDATE items SET reservationID=NULL, borrowerID='"+studentNum
			+"' WHERE itemNum IN "+itemNums);
	}
	
	public void reserveSelectedItems(ArrayList<ItemInfo> itemInfos, String studentNum) {
		ArrayList<String> itemNumsList = new ArrayList<String>();
		for (int i = 0; i < itemInfos.size(); i++) {
			itemNumsList.add("\""+itemInfos.get(i).getItemNum()+"\"");
		}
		String itemNums = itemNumsList.toString().replace("[", "(").replace("]", ")")
			.replace(", ", ",");
		dbConnector.executeUpdate("UPDATE items SET reservationID='"+studentNum
			+"' WHERE itemNum IN "+itemNums);
	}
	
	public String returnSelectedItems(ArrayList<ItemInfo> itemInfos) {
		ArrayList<String> itemNumsList = new ArrayList<String>();
		for (int i = 0; i < itemInfos.size(); i++) {
			itemNumsList.add("\""+itemInfos.get(i).getItemNum()+"\"");
		}
		String itemNums = itemNumsList.toString().replace("[", "(").replace("]", ")")
			.replace(", ", ",");
		dbConnector.executeUpdate("UPDATE items SET borrowerID=NULL"
			+" WHERE itemNum IN "+itemNums);
		return itemNums;
	}
	
	public void insertToItemsHistory(ArrayList<ItemInfo> itemInfos, String studentNum) {
		String sql = "SELECT CONCAT(firstName,' ',lastName) AS studentName "
				+ "FROM borrowers WHERE studentNum='"+studentNum+"'";
		ArrayList<String> studentName = dbConnector.executeQuery(sql, "select student name");
		String sqlInsert = "INSERT INTO items_history "
			+ "(itemNum, itemName, state, borrowerName, borrowerID) VALUES ";
		for (int i = 0; i < itemInfos.size(); i++) {
			sqlInsert += "('"+itemInfos.get(i).getItemNum()+"','"+itemInfos.get(i).getItemName()
			+ "','"+itemInfos.get(i).getState()+"','"+studentName.get(0)+"','"+studentNum+"'),";
		}
		sqlInsert = sqlInsert.substring(0, sqlInsert.length()-1);
		dbConnector.executeUpdate(sqlInsert);
	}
	
	public void setDateReturned(String returnedItemNums) {
		String sql = "UPDATE items_history SET dateReturned=now() "
			+ "where dateReturned IS NULL AND itemNum IN "+returnedItemNums;
		dbConnector.executeUpdate(sql);
	}
	
	public ArrayList<String> getBorrowedItems(String studentNum) {
		String sql = "SELECT itemNum, ItemName, state FROM items "
				+ "WHERE borrowerID='"+studentNum+"'";
		ArrayList<String> strings = 
			(dbConnector.executeQuery(sql, "select borrowed items by current borrower"));
		return strings;
	}
	
	public ArrayList<String> getBorrowedItemsAllColumns(String studentNum) {
		String sql = "SELECT * FROM items "
				+ "WHERE borrowerID='"+studentNum+"'";
		ArrayList<String> strings = 
			(dbConnector.executeQuery(sql, "select borrowed items by current borrower (all columns)"));
		return strings;
	}
	
	public ArrayList<String> getBorrower(String studentNum) {
		String sql = "SELECT * FROM borrowers WHERE studentNum='"
			+studentNum+"'";
		ArrayList<String> strings = 
			(dbConnector.executeQuery(sql, "select borrower"));
		return strings;
	}
	
	public void deleteBorrowers(ArrayList<String> studentNums) {
		for (int i = 0; i < studentNums.size(); i++) {
			studentNums.set(i, ("'"+studentNums.get(i)+"'"));
		}
		String studentNumsStr = studentNums.toString().replace("[", "(").replace("]", ")")
			.replace(", ", ",");
		String sqlForItems = "UPDATE items SET reservationID=NULL, borrowerID=null "
			+ "WHERE reservationID IN "+studentNumsStr+" OR borrowerID IN "+studentNumsStr;
		if (studentNumsStr.length()>2) {
			dbConnector.executeUpdate(sqlForItems);
			String sql = "DELETE FROM borrowers WHERE studentNum IN "
				+ studentNumsStr;
			dbConnector.executeUpdate(sql);
		}
	}
	
	public void editBorrower(ArrayList<String> editInfos) {
		if (!editInfos.isEmpty()) {
			String sql = "UPDATE borrowers SET firstName=\""+editInfos.get(0)+"\","
				+ "lastName=\""+editInfos.get(1)+"\",studentNum=\""+editInfos.get(2)
				+ "\",contactNum=\""+editInfos.get(3)+"\",teamName=\""+editInfos.get(4)
				+ "\" WHERE studentNum='"+editInfos.get(5)+"'";
			dbConnector.executeUpdate(sql);
		}
	}
	
	public void editItem(ArrayList<String> value) {
		if (!value.isEmpty()) {
			String sql = "UPDATE items SET itemNum=\""+value.get(0)
				+"\", itemName=\""+value.get(1)+"\", state=\""+value.get(2)
				+"\" WHERE itemNum='"+value.get(3)+"'";
			dbConnector.executeUpdate(sql);
		}
	}
	
	public void addItem(String[] item) {
		String sql = "INSERT INTO items (itemNum,itemName,state) VALUES (\""
			+ item[0]+"\",\""+item[1]+"\",\""+item[2]+"\")";
		dbConnector.executeUpdate(sql);
	}
	
	public void deleteItems(ArrayList<String> itemNums) {
		if (!itemNums.isEmpty()) {
			for (int i = 0; i < itemNums.size(); i++) {
				itemNums.set(i, ("'"+itemNums.get(i)+"'"));
			}
			String itemNumsStr = itemNums.toString().replace("[", "(").replace("]", ")")
				.replace(", ", ",");
			String sql = "DELETE FROM items WHERE itemNum IN "
				+ itemNumsStr;
			dbConnector.executeUpdate(sql);
		}
	}
	
	public void closeDBConnection() {
		dbConnector.close();
	}
}
