package cmsc127.project.upseis.infoclasses;

public class AdministratorInfo {
	private String empFirstName;
	private String empLastName;
	private String empPassword;
	private String empNumber;
	
	public void setEmpFirstName(String empFirstName) {
		this.empFirstName = empFirstName;
	}
	public void setEmpLastName(String empLastName) {
		this.empLastName = empLastName;
	}
	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}
	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}
	public String getEmpFirstName() {
		return empFirstName;
	}
	public String getEmpLastName() {
		return empLastName;
	}
	public String getEmpPassword() {
		return empPassword;
	}
	public String getEmpNumber() {
		return empNumber;
	}
} 