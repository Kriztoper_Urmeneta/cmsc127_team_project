package cmsc127.project.upseis.infoclasses;

public class BorrowerInfo {
	private String currentBorrower;
	
	private String studentNum;
	private String contactNum;
	private String firstName;
	private String lastName;
	private String teamName;
	
	public void setStudentNum(String studentNum) {
		this.studentNum = studentNum;
	}
	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getStudentNum() {
		return studentNum;
	}
	public String getContactNum() {
		return contactNum;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getTeamName() {
		return teamName;
	}
	public String getCurrentBorrower() {
		return currentBorrower;
	}
	public void setCurrentBorrower(String currentBorrower) {
		this.currentBorrower = currentBorrower;
	}
}