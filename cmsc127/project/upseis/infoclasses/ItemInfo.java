package cmsc127.project.upseis.infoclasses;

public class ItemInfo {
	private String itemNum;
	private String itemName;
	private String state;
	private String reservationId;
	
	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}
	public String getItemNum() {
		return itemNum;
	}
	public String getItemName() {
		return itemName;
	}
	public String getState() {
		return state;
	}
	public String reservationId() {
		return reservationId;
	}
}